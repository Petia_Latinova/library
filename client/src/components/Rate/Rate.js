import React from 'react';
import ReactStars from "react-rating-stars-component";
import { BASE_URL } from '../../common/constants';
import { store } from 'react-notifications-component';

const Rate = (props) => {

  const ratingChanged = (newRating) => {
    fetch(`${BASE_URL}/api/books/${props.bookId}/rate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ rate: newRating }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        props.refreshBooks();
        store.addNotification({
          title: 'Success',
          message: 'You rated successfully',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }

  return (
    <div>
      <ReactStars
        count={5}
        onChange={ratingChanged}
        size={30}
        activeColor="#ffd700"
      />
    </div>
  );
}

export default Rate;