import React, { useState, useEffect } from 'react';
import ReactStars from "react-rating-stars-component";
const RateDetails = (props) => {

  const calcRating = () => {
    if (props.Rate.length === 0) {
      return 0;
    }
    return props.Rate.reduce((sum, el) => sum + Number(el.rate), 0) / props.Rate.length;
  }

  const [rating] = useState({
    size: 23,
    edit: false,
    color: "#bababa",
    activeColor: "#c76aab",
    isHalf: true,
    count: 5,
  });
  const [value, setValue] = useState(calcRating());

  useEffect(() => {
    const val = props.Rate.reduce((sum, el) => sum + Number(el.rate), 0) / props.Rate.length;
    setValue(val);
  }, [props.Rate]);

  return (
    <div>
      <ReactStars {...rating} value={value} />
    </div>
  );
}
export default RateDetails;