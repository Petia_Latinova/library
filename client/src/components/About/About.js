import React from 'react';

import '../../static/css/About.css';

import Programmer from '../../static/img/undraw_programmer.svg';
import { Row, Card, CardBody, Button, CardText } from 'reactstrap';

const About = (props) => {

  const openLink = (link) => {
    switch (link) {
      case 'nodejs':
        window.location.href = "https://nodejs.org/";
        break
      case 'express':
        window.location.href = "https://expressjs.com/";
        break
      case 'nestjs':
        window.location.href = "https://nestjs.com/";
        break
      case 'jwt':
        window.location.href = "https://jwt.io/";
        break
      case 'passport':
        window.location.href = "http://www.passportjs.org/";
        break
      case 'bcryptjs':
        window.location.href = "https://www.npmjs.com/package/bcryptjs";
        break;
      case 'mariadb':
        window.location.href = "https://mariadb.org/";
        break;
      case'typeorm':
        window.location.href = "https://typeorm.io/";
        break;
      case 'reactjs':
        window.location.href = "https://reactjs.org/";
        break;
      case 'typescript':
        window.location.href = "https://www.typescriptlang.org/";
        break;
      default:
        break;
    }
  }

  return (
    <div>
      <div className="text-center about-title">About Library</div>
      <div className="text-center about-subtitle">Created to allow people to find more books suitable to their taste</div>
      <Row className="justify-content-md-center">
        <img className="justify-content-md-center undraw-programmer" src={Programmer} style={{ width: 800 }} alt="Not Found"/>
      </Row>
      <div className="text-center features-title">Features</div>

      <Card className="flex-row card-about">
        <CardBody>

          <CardText className="text-center">
            <Button className="btn-about" color="primary" onClick={() => openLink('nodejs')}>Node.js</Button> as a backend framework
          </CardText>
        </CardBody>
      </Card>

      <Card className="flex-row card-about">
        <CardBody>
          
          <CardText className="text-center">
            <Button className="btn-about bg-gradient-theme-left login border-0" onClick={() => openLink('nestjs')}>NestJS</Button>and
            <Button className="btn-about bg-gradient-theme-left login border-0" onClick={() => openLink('express')}>Express</Button>as the web framework on the server
          </CardText>
        </CardBody>
      </Card>

      <Card className="flex-row card-about">
        <CardBody >
          <CardText className="text-center">
            Implements stateless authentication with<Button className="btn-about" color="info" onClick={() => openLink('jwt')}>JWT</Button>tokens
          </CardText>
        </CardBody>
      </Card>

      <Card className="flex-row card-about">
        <CardBody >
          <CardText className="text-center">
            Authenticates<Button className="btn-about" color="info" onClick={() => openLink('jwt')}>JWT</Button>and authentication using
            <Button className="btn-about" color="danger" onClick={() => openLink('passport')}>passport</Button>
          </CardText>
        </CardBody>
      </Card>

      <Card className="flex-row card-about">
        <CardBody >
          <CardText className="text-center">
            Hashes passwords using the<Button className="btn-about" color="success" onClick={() => openLink('bcryptjs')}>bcryptjs</Button>package
          </CardText>
        </CardBody>
      </Card>
       
      <Card className="flex-row card-about">
        <CardBody >
          <CardText className="text-center">
            <Button className="btn-about" color="danger" onClick={() => openLink('mariadb')}>MariaDB</Button>and
            <Button className="btn-about" color="secondary" onClick={() => openLink('typeorm')}>TypeORM</Button>is used for storing and querying data
          </CardText>
        </CardBody>
      </Card>

      <Card className="flex-row card-about">
        <CardBody >
          <CardText className="text-center">
            <Button className="btn-about" color="primary" onClick={() => openLink('reactjs')}>React JS</Button>is used as the frontend framework
          </CardText>
        </CardBody>
      </Card>
    </div>
  );
}

export default About;