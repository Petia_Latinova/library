import React, { useState, useEffect } from 'react';
import { FormGroup, Label, Input, Col, Row, Button } from 'reactstrap';
import AdminPhotoPannel from '../../static/img/admin-profile-photo.png';
import '../../static/css/Admin.css';
import { BASE_URL } from '../../common/constants';
import Loader from '../Loader/Loader';
import CreateBook from '../Books/CreateBook';
import { store } from 'react-notifications-component';

const Admin = () => {

  const [dateText, setDateText] = useState(new Date().toISOString().split("T")[0]);
  const [timeText, setTimeText] = useState("00:00:00");
  const [descriptionText, setDescriptionText] = useState("");
  const [userName, setUserName] = useState("");
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [usernameValid, setUsernameValid] = useState(false);

  const handleDateChange = (e) => {
    setDateText(e.target.value);
  }

  const handleTimeChange = (e) => {
    setTimeText(e.target.value);
  }

  const handleUserNameChange = (e) => {
    setUserName(e.target.value);
    if (users.filter(o => o.name === e.target.value).length > 0) {
      setUsernameValid(true);
    }
    else {
      setUsernameValid(false);
    }
  }

  const handleDescriptionChange = (e) => {
    setDescriptionText(e.target.value);
  }

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/api/admin/users`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then(r => r.json())
      .then(users => {
        if (users.error) {
          throw new Error(users.message);
        }

        setUsers(users);
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .finally(() => setLoading(false));
  }, []);

  const banUser = () => {
    const result = window.confirm("Are you sure you want to ban this user?");
    if (result === false) {
      return;
    }

    const user = users.find(o => o.name === userName);
    if (!user) {
      store.addNotification({
        title: 'Error',
        message: `User can't be found`,
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
      return;
    }
    setLoading(true);
    fetch(`${BASE_URL}/api/admin/users/${user.id}/banstatus`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({
        banned: `${dateText} ${timeText}`,
        description: descriptionText,
        userId: user.id,
      }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.message) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        store.addNotification({
          title: 'Success',
          message: 'User is banned!',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .catch((ex) => store.addNotification({
        title: 'Error',
        message: ex.message,
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      }))
      .finally(() => setLoading(false));
  }

  const deleteUser = () => {
    const result = window.confirm("Are you sure you want to delete this user?");
    if (result === false) {
      return;
    }

    const user = users.find(o => o.name === userName);
    if (!user) {
      store.addNotification({
        title: 'Error',
        message: `User can't be found`,
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
      return;
    }

    setLoading(true);
    fetch(`${BASE_URL}/api/admin/users/${user.id}/`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({
        isDeleted: true,
      }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.message) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        store.addNotification({
          title: 'Success',
          message: 'User is deleted!',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .finally(() => setLoading(false));
  }

  return (
    <>
      { loading ? <h1><Loader /></h1> : null}  
      <CreateBook /><br /><br />
      <hr/>
      <br />
      <Row className="admin-container">
        <Col className="col-md-6">
          <FormGroup>
            <Label for="exampleText">Search User</Label>
            <Input valid={usernameValid ? true : false} className="col-md-11 mb-2" placeholder="Search..." onChange={handleUserNameChange} />
          </FormGroup>
        </Col>
        <Col className="col-md-2">
          <Button className="btn-delete-user" color="danger" onClick={() => deleteUser()}>Delete User</Button>
        </Col>
      </Row>

      <Row className="admin-container">
        <Col className="col-md-4">
          <FormGroup>
            <Label for="exampleText">Description</Label>
            <Input className="admin-description" type="textarea" name="text" onChange={handleDescriptionChange} />
          </FormGroup>
        </Col>

        <Col className="col-md-2">
          <FormGroup className="admin-date">
            <Label for="exampleDate">Date until to bann</Label>
            <Input
              type="date"
              name="date"
              id="exampleDate"
              placeholder="date placeholder"
              onChange={handleDateChange}
              min={new Date().toISOString().split("T")[0]}
              defaultValue={new Date().toISOString().split("T")[0]}
            />
          </FormGroup>
          <Row>
            <Col >
              <FormGroup className="admin-time">
                <Label for="exampleTime">Time</Label>
                <Input
                  type="time"
                  name="time"
                  id="exampleTime"
                  onChange={handleTimeChange}
                  placeholder="time placeholder"
                  defaultValue="00:00:00"
                />
              </FormGroup>
            </Col>
          </Row>
        </Col>
        
        <Col className="col-md-2">
          <FormGroup>
            <Button className="btn-ban-user" color="danger" onClick={() => banUser()}>Ban User</Button>
          </FormGroup>
        </Col>
      </Row>
      <Col>
        <img className="img-in-admin" alt="Not Found" src={AdminPhotoPannel} />
      </Col>
      <br />
    </>
  );
}

export default Admin;