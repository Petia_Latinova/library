import React from 'react'
import { Dropdown } from 'semantic-ui-react'

//import 'semantic-ui-css/semantic.min.css'
import '../../static/css/UserSearch.css';
import { BASE_URL } from '../../common/constants'

const UserSearch = (props) => {

  const onChange = (e, data) => {
    e.preventDefault();
    props.setUserId(data.value);
  }

  const userOptions = props.users.map(o => {
    return { key: o.id, value: o.id, text: o.name, image: { avatar: true, src: `${BASE_URL}/avatars/${o.avatarPath}` } }
  });

  //require('semantic-ui-css/semantic.min.css')
  return (
    <Dropdown
      placeholder='Select User'
      fluid
      search
      selection
      options={userOptions}
      onChange={onChange}
    />
  )
}

export default UserSearch