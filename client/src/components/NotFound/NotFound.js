import React from 'react';

import ErrorPhoto from '../../static/img/404ERROR.png';

import '../../static/css/NotFound.css';

const NotFound = (props) => {
    return (
    <div>
            <img className="img-error" alt="Not Found" src={ErrorPhoto} />
    </div>
    )
};

export default NotFound;
