import React from 'react';
import { AiOutlineDislike, AiOutlineLike } from 'react-icons/ai';
import '../../static/css/Votes.css';
import { Button } from 'reactstrap';
import { BASE_URL } from '../../common/constants';
import { store } from 'react-notifications-component';

const Votes = (props) => {

  const handleVote = (like, id) => {
    const vote = { liked: like };
    fetch(`${BASE_URL}/api/reviews/${id}/votes`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify(vote),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }

        props.refreshReview();        
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }

  return (
    <div>
      {props.canVote &&
        <div>
        <Button className="btn-like" outline color="success" onClick={() => handleVote(true, props.reviewId)}>
          <span className="vote-text"> <AiOutlineLike /></span> <span className="vote-number-like">{props.likes}</span>
          </Button >
        <Button className="btn-dislike" outline color="danger" onClick={() => handleVote(false, props.reviewId)}>
          <span className="vote-text"><AiOutlineDislike /></span> <span className="vote-number-dislike">{props.dislikes}</span> 
          </Button >
        </div>}

      {!props.canVote &&
        <div>
        <Button className="btn-like-disable" outline color="success" disabled>
            <span className="vote-text"> <AiOutlineLike /></span> <span className="vote-number-like">{props.likes}</span>
          </Button >
        <Button className="btn-dislike-disable" outline color="danger" disabled>
            <span className="vote-text"><AiOutlineDislike /></span> <span className="vote-number-dislike">{props.dislikes}</span>
          </Button >
        </div>}
    </div>
  );
}

export default Votes;