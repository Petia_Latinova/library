import React from 'react';
import { Nav } from 'react-bootstrap';
import { Button } from 'reactstrap';
import Typed from 'react-typed';

import '../../static/css/Home.css';
import '../../styles/keyframes.scss';

import '../../styles/_base.scss';

import MagicBook from '../../static/img/magic_book.png';

const Home = (props) => {

  return (
    <div>
      <div className="wrapper title-home-moving">
        <Typed 
          strings={[
            'Experience The magic of books',
            'And the magic of react',
            'For magic lovers']}
          typeSpeed={100}
          loop
          showCursor
          cursorChar="|"
        >
      </Typed>
      </div>
      <div className="welcome-text"> Wellcome to our online library! Read books! Connect with new friends and have fun!</div>
      <div className="row wrapper">
        <div className="col-md-6">
          <Nav.Link href="/about"><Button color="primary" className="btn-home-about btn-main--anim">Learn More</Button></Nav.Link>
        </div>
        <div className="col">
          <Nav.Link href="/signup"><Button color="primary" className="btn-home-sign-up btn-main--anim btn-reg bg-gradient-theme-left">Register Now</Button></Nav.Link>
        </div>
      </div>
      <div className="wrapper">
        <img className="magic-book" alt="Not Found" src={MagicBook} style={{ width: 800 }}/>
      </div>
    </div>);

}

export default Home;