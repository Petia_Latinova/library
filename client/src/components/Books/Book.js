import React, { useContext } from 'react';
import { BASE_URL } from '../../common/constants';
import { Card, CardBody, CardImg, CardText, CardTitle, Button, Col } from 'reactstrap';
import propTypes from 'prop-types';
import '../../static/css/Books.css';
import { useHistory } from 'react-router-dom';
import Rate from '../Rate/Rate';
import RateDetails from '../Rate/RateDetails';
import AuthContext from '../../providers/AuthContext';
import { store } from 'react-notifications-component';

const Book = (props) => {
  const history = useHistory();
  const { user } = useContext(AuthContext);
  const refreshBooks = () => { props.getBooks() }

  const borrowBook = () => {

    fetch(`${BASE_URL}/api/books/${props.book.id}/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({
        isBorrowed: true,
        userId: props.book.userId,
      }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }
        refreshBooks();
        store.addNotification({
          title: 'Success',
          message: 'Book is borrowed!',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      });
  };


  const returnBook = () => {

    fetch(`${BASE_URL}/api/books/${props.book.id}/`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }
        refreshBooks();
        store.addNotification({
          title: 'Success',
          message: 'Book is returned!',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      });
  }

  const deleteBook = () => {
    const result = window.confirm("Are you sure you want to delete this book?");
    if (result === false) {
      return;
    }

    fetch(`${BASE_URL}/api/admin/books/${props.book.id}/`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
        },
      })
      .then(r => r.json())
      .then(result => {
        if (!result) {
          return store.addNotification({
              title: 'Error',
              message: result.message,
              type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
              container: 'top-right',                // where to position the notifications
              animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
              dismiss: {
                duration: 3000
              }
            });
        }

        store.addNotification({
          title: 'Success',
          message: 'Book is deleted!',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
        history.push('/books');
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }

  return (
    <>
      <Col md={6} sm>
        <Card className="flex-row book-card" >
          <div>
            <CardImg className="card-img-left" src={`${BASE_URL}/covers/${props.book.photoPath}`} style={{ width: 'auto', height: 350 }} onClick={() => history.push(`/books/${props.book.id}`)} />
          </div>
          <CardBody> 
            <CardText className="book-title">{props.book.title}</CardText>
            <CardTitle>by {props.book.author}</CardTitle>
            <RateDetails bookId={props.book.id} Rate={props.book.Rate} />
            <div className="card-action">
              <div className="rate">
                <label>Rate this book</label>
                <Rate bookId={props.book.id} refreshBooks={refreshBooks} />
              </div>
              <div className="d-flex">
                {!props.book.isBorrowed && <Button className="bg-gradient-theme-left login border-0" color="primary" onClick={() => borrowBook()}>Borrow</Button>}
                {(props.book.isBorrowed && props.book.userId === user.id) && <Button className="bg-gradient-theme-left login border-0" color="primary"  onClick={() => returnBook()}>Return</Button>}
                {(props.book.isBorrowed && props.book.userId !== user.id) && <div className="book-borrowed">The book is borrowed</div>}
                {(props.showControls || user.role === "Admin") && <Button color="link" onClick={() => deleteBook()}>Delete</Button>}
              </div>
            </div>
          </CardBody>
        </Card>
        <br />
      </Col>
    </>
  );

};

Book.propTypes = {
  book: propTypes.object.isRequired,
};

export default Book;