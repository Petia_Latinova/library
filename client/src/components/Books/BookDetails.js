
import React, { useState, useEffect, useContext } from 'react';
import { BASE_URL } from '../../common/constants';
import Reviews from '../Reviews/Reviews';
import Book from '../Books/Book';
import UpdateBook from './UpdateBook';
import AuthContext from '../../providers/AuthContext';
import { store } from 'react-notifications-component';
import { Row } from 'reactstrap';

const BookDetails = (props) => {

  const match = props.match;
  const bookID = match.params.id;
  const [data, setData] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const { user } = useContext(AuthContext);

  const getBooks = () => {
    setRefresh(!refresh);
  }

  useEffect(() => {

    const url = `${BASE_URL}/api/books/${bookID}`;
    fetch(url,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
        }
      })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        setData(result);
      })
  }, [bookID, refresh]);

  return (
    <>
    <br />
      {data &&
        <div>
          <Row>
            <Book key={data.id} book={data} bookID={data.id} getBooks={getBooks} />
            {(props.showControls || user.role === "Admin") &&
            <UpdateBook bookId={bookID} title={data.title} author={data.author} getBooks={getBooks} />}
          </Row>
      </div>}
      <Reviews bookId={bookID} />
      <br/>
    </>
  );
}

export default BookDetails;
