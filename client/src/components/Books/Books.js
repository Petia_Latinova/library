import React, { useEffect, useState, Fragment, useRef, useCallback } from 'react';
import { BASE_URL } from '../../common/constants';
import Loader from '../Loader/Loader';
import { Row } from 'reactstrap';
import Carosel from '../Carosel/Carosel';
import '../../static/css/AppError.css';
import '../../static/css/Books.css';
import BookSimple from './BookSimple';
import SearchInput from '../Search/SearchInput';
import { store } from 'react-notifications-component';

const Books = () => {
  const limit = 10;

  const [loading, setLoading] = useState(false);
  const [books, setBooks] = useState([]);
  const [page, setPage] = useState(0);
  const [query, setQuery] = useState('');
  const [hasMore, setHasMore] = useState(true);


  const search = (text) => {
    setHasMore(true);
    setPage(0);
    setQuery(text);
  }

  useEffect(() => {
    setLoading(true);
    let url = `${BASE_URL}/api/books`;
    if (query && query !== '') {
      url += `?search=${query}&page=${page}&limit=${limit}`;
    }
    else {
      url += `?page=${page}&limit=${limit}`;
    }

    fetch(url,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
        }
      })
      .then(result => {
        //setLoading(false)
        return result.json();
      })
      .then(result => {
        if (Array.isArray(result)) {
          if(result.length === 0) {
            setHasMore(false);
          }
          else if (page === 0) {
            setBooks(result);
          }
          else {
            setBooks(prevBooks => [...prevBooks, ...result]);
          }
        }
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .finally(() => setLoading(false));
  }, [query, page]);

  const observer = useRef()
  const lastBookElementRef = useCallback(node => {
    if (loading) return
    if (observer.current) observer.current.disconnect()
    observer.current = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting && hasMore) {
        setPage(page => page + 1);
      }
    })
    if (node) observer.current.observe(node)
  }, [loading, hasMore])

  return (
    <Fragment>
      <Carosel />
      <div className="search-books-container">
        <SearchInput search={search} />
      </div>
      <div className="books-container">
        <Row> {books
          .map((book, index) => index + 1 === books.length ?
            <div key={book.id} ref={lastBookElementRef}>
              <BookSimple key={book.id} book={book} bookID={book.id} borrow={book.isBorrowed} />
            </div>
            :
            <BookSimple key={book.id} book={book} bookID={book.id} borrow={book.isBorrowed} />)}
        </Row>
        {
          loading
            ? <h1><Loader /></h1>
            : null}
      </div>
    </Fragment>
  );
}
export default Books;