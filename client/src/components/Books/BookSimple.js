import React, { Fragment } from 'react';
import { BASE_URL } from '../../common/constants';
import { Card, CardImg, Col } from 'reactstrap';
import propTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import RateDetails from '../Rate/RateDetails';

import '../../static/css/Books.css';

const BookSimple = ({ book }) => {

  const history = useHistory();

  return (
    <Col className="book-simple justify-content-between">
      <Fragment>
        <Card>
          {book.photoPath === 'noCover.jpg' && <span className="book-author-no-cover">Author: {book.author}</span>}
          <CardImg className="card-img-left book-img" src={`${BASE_URL}/covers/${book.photoPath}`} style={{ width: 'auto', height: 250 }} onClick={() => history.push(`/books/${book.id}`)} />
          {book.photoPath === 'noCover.jpg' && <span className="book-title-no-cover">Title: {book.title}</span>}
          <div className="wrapper">
            <RateDetails  bookId={book.id} Rate={book.Rate} />
          </div>
        </Card>
        <br />
      </Fragment>
    </Col>
  );

};

BookSimple.propTypes = {
  book: propTypes.object.isRequired,
};

export default BookSimple;