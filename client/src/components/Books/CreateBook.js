import React, { useState } from 'react';
import { Button, Label } from 'reactstrap';
import '../../static/css/Admin.css';
import { BASE_URL } from '../../common/constants';
import { Form, FormGroup, Row, Col } from 'react-bootstrap';
import { store } from 'react-notifications-component';
import '../../static/css/Books.css';

const CreateBook = () => {

  const [title, setTitle] = useState('');
  const [author, setAuthor] = useState('');
  const updateTitle = value => setTitle(value);
  const updateAuthor = value => setAuthor(value);
  const [files, setFiles] = useState([]);

  const uploadCover = (book) => {
    const formData = new FormData();
    if (!files.length) {
      store.addNotification({
        title: 'Error',
        message: 'No choosed file',
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
      return;
    }

    formData.append('files', files[0]);
    fetch(`${BASE_URL}/api/admin/books/${book.id}/cover`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: formData,
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        store.addNotification({
          title: 'Success',
          message: 'Book is created',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  };

  const createBook = () => {
    fetch(`${BASE_URL}/api/admin/books`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ author: author, title: title })
    })
      .then(result => result.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        uploadCover(result);
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }

  return (
    <Form>
      <Row className="create-book-container">
        <Col className="col-md-4">
          <FormGroup controlId="title">
            <Label>Book title</Label>
            <Form.Control type="text" placeholder="Enter Title" value={title} onChange={e => updateTitle(e.target.value)} />
          </FormGroup>
        </Col>
        <Col className="col-md-4">
          <FormGroup controlId="author">
            <Label>Book author</Label>
            <Form.Control type="text" placeholder="Enter Author" value={author} onChange={e => updateAuthor(e.target.value)} />
          </FormGroup>
        </Col>
          <FormGroup>
            <Label>Upload cover</Label><br />
            <input type="file" onChange={e => setFiles(Array.from(e.target.files))} />
            <Button variant="outline-success" color="primary" onClick={createBook}>Create</Button>
          </FormGroup>
      </Row>
    </Form>

  )
}
export default CreateBook;