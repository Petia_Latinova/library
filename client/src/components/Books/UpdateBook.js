import React, { useState} from 'react';
import { Button, Col } from 'reactstrap';
import '../../static/css/Admin.css';
import { BASE_URL } from '../../common/constants';
import { Form } from 'react-bootstrap';
import { store } from 'react-notifications-component';

const UpdateBook = (props) => {
  const [title, setTitle] = useState(props.title);
  const [author, setAuthor] = useState(props.author);
  const updateTitle = value => setTitle(value);
  const updateAuthor = value => setAuthor(value);
  const [files, setFiles] = useState([]);

  const uploadCover = () => {
    const formData = new FormData();
    
    formData.append('files', files[0]);
    fetch(`${BASE_URL}/api/admin/books/${props.bookId}/cover`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: formData,
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        store.addNotification({
          title: 'Success',
          message: 'Book is updated',
          type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
        refreshBooks();
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  };

  const refreshBooks = () => {
    props.getBooks();
  }
  const updateBook = () => {
    fetch(`${BASE_URL}/api/admin/books/${props.bookId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ author: author, title: title })
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        uploadCover();
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }

  return (
    <Col md={6} sm>
      <h4>Edit Book</h4>
      <Form>
        <Form.Group controlId="title">
          <Form.Label className="col-form-label-sm">Book title</Form.Label>
          <Form.Control type="text" placeholder="Enter Title" value={title} onChange={e => updateTitle(e.target.value)} />
        </Form.Group>
        <Form.Group controlId="author">
          <Form.Label className="col-form-label-sm">Book author</Form.Label>
          <Form.Control type="text" placeholder="Enter Author" value={author} onChange={e => updateAuthor(e.target.value)} />
        </Form.Group>
        <Form.Group controlId="cover">
          <Form.Label className="col-form-label-sm">Upload cover</Form.Label><br />
          <input type="file" onChange={e => setFiles(Array.from(e.target.files))} />
        </Form.Group>
        <Button className="bg-gradient-theme-left login border-0" color="primary" variant="outline-success" onClick={() => updateBook()}>Edit</Button>

      </Form>
    </Col>
  )
}
export default UpdateBook;