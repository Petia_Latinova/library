import AuthForm, { STATE_LOGIN } from './AuthForm';
import React from 'react';
import { Card, Col, Row } from 'reactstrap';

import '../../static/css/Login.css';

const Login = (props) => {

  const handleAuthState = authState => {
    if (authState === STATE_LOGIN) {
      props.history.push('/login');
    } else {
      props.history.push('/signup');
    }
  };

  return (
    <Row
      style={{
        width: '100%',
        height: '80vh',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Col md={4} lg={3}>
        <Card body>
          <AuthForm
            authState={props.authState}
            onChangeAuthState={handleAuthState}
          />
        </Card>
      </Col>
    </Row>
  );
}

export default Login;
