import BookLogoLoginReg from '../../static/img/login_reg_logo.png';
import PropTypes from 'prop-types';
import React, { useContext, useState } from 'react';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import { BASE_URL } from '../../common/constants';
import JwtDecode from 'jwt-decode';
import AuthContext from '../../providers/AuthContext';
import { useHistory } from 'react-router-dom';
import { store } from 'react-notifications-component';

const AuthForm = (props) => {

  const { setLoginState } = useContext(AuthContext);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmpassword] = useState("");
  const [files, setFiles] = useState([]);
  const history = useHistory();
  const [usernameValid, setUsernameValid] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);
  const [confirmPasswordValid, setConfirmpasswordValid] = useState(false);

  const isLogin = () => {
    return props.authState === STATE_LOGIN;
  }

  const isSignup = () => {
    return props.authState === STATE_SIGNUP;
  }

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
    if (e.target.value.length > 2 && e.target.value.length < 26) {
      setUsernameValid(true);
    }
    else {
      setUsernameValid(false);
    }
  }

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
    if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/.test(e.target.value)) { // /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/
      setPasswordValid(true);
    }
    else {
      setPasswordValid(false);
    }
  }

  const handleConfirmPasswordChange = (e) => {
    setConfirmpassword(e.target.value);
    if (passwordValid && e.target.value === password) {
      setConfirmpasswordValid(true);
    }
    else {
      setConfirmpasswordValid(false);
    }
  }

  const uploadAvatar = (id) => {
    const formData = new FormData();
    if (!files.length) {
      history.push('/login');
      return;
    }

    formData.append('files', files[0]);
    fetch(`${BASE_URL}/api/users/${id}/avatar`, {
      method: 'POST',
      headers: {
      },
      body: formData,
    })
      .then(r => r.json())
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
      .finally(() => history.push('/login')); //TODO change picture in payload
  };

  const fetchLogin = (user) => {
    if (!username) {
      return store.addNotification({
        title: 'Error',
        message: 'Invalid username!',
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }
    if (!password) {
      return store.addNotification({
        title: 'Error',
        message: 'Invalid password!',
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }

    fetch(`${BASE_URL}/api/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }

        try {
          const payload = JwtDecode(result.token);
          setLoginState({ isLoggedIn: true, user: payload });
        } catch (ex) {
          return store.addNotification({
            title: 'Error',
            message: ex.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }

        localStorage.setItem('token', result.token);
        history.push('/books');
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }

  const fetchRegister = (user) => {
    if (!username) {
      return store.addNotification({
        title: 'Error',
        message: 'Invalid username!',
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }

    if (!password) {
      return store.addNotification({
        title: 'Error',
        message: 'Invalid password!',
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
    }

    if (password !== confirmPassword) {
      store.addNotification({
        title: 'Error',
        message: `Your passwords doesn't match!`,
        type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right',                // where to position the notifications
        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
        dismiss: {
          duration: 3000
        }
      });
      return;
    }
    fetch(`${BASE_URL}/api/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        ...user,
        name: username,
      }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }

        uploadAvatar(result.id);
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }
  const changeAuthState = authState => event => {
    event.preventDefault();

    props.onChangeAuthState(authState);
  };

  const handleSubmit = event => {
    event.preventDefault();
    const user = {
      name: username,
      password: password
    }
    if (props.authState === STATE_LOGIN) {
      fetchLogin(user);
    }
    else {
      fetchRegister(user);
    }
  };

  const renderButtonText = () => {
    const { buttonText } = props;

    if (!buttonText && isLogin()) {
      return 'Login';
    }

    if (!buttonText && isSignup()) {
      return 'Signup';
    }

    return buttonText;
  }

  const {
    showLogo,
    usernameLabel,
    usernameInputProps,
    passwordLabel,
    passwordInputProps,
    confirmPasswordLabel,
    confirmPasswordInputProps,
    avatarLabel,
    children,
    onLogoClick,
  } = props;

  return (
    <Form onSubmit={handleSubmit}>
      {showLogo && (
        <div className="text-center pb-4">
          <img
            src={BookLogoLoginReg}
            className="rounded"
            style={{ height: 80 }}
            onClick={onLogoClick}
            alt="Not Found"
          />
        </div>
      )}
      <FormGroup>
        <Label for={usernameLabel}>{usernameLabel}</Label>
        <Input valid={usernameValid ? true : false} {...usernameInputProps} onChange={handleUsernameChange} />
      </FormGroup>
      {isSignup() && (
        <FormGroup>
          <Label for={avatarLabel}>{avatarLabel}</Label><br />
          <input type="file" onChange={e => setFiles(Array.from(e.target.files))} />
        </FormGroup>
      )}
      <FormGroup>
        <Label for={passwordLabel}>{passwordLabel}</Label>
        <Input valid={passwordValid ? true : false} {...passwordInputProps} onChange={handlePasswordChange} />
      </FormGroup>
      {isSignup() && (
        <FormGroup>
          <Label for={confirmPasswordLabel}>{confirmPasswordLabel}</Label>
          <Input valid={confirmPasswordValid ? true : false} {...confirmPasswordInputProps} onChange={handleConfirmPasswordChange} />
        </FormGroup>
      )}
      {isSignup() && <FormGroup check>
      </FormGroup>}
      <hr />
      <Button
        size="lg"
        className="bg-gradient-theme-left login border-0"
        color="primary"
        block
        onClick={handleSubmit}>
        {renderButtonText()}
      </Button>

      <div className="text-center pt-1">
        <h6>or</h6>
        <h6>
          {isSignup() ? (
            <a href="#login" onClick={changeAuthState(STATE_LOGIN)}>
              Login
            </a>
          ) : (
              <a href="#signup" onClick={changeAuthState(STATE_SIGNUP)}>
                Signup
              </a>
            )}
        </h6>
      </div>

      {children}
    </Form>
  );
}

export const STATE_LOGIN = 'LOGIN';
export const STATE_SIGNUP = 'SIGNUP';

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP]).isRequired,
  showLogo: PropTypes.bool,
  usernameLabel: PropTypes.string,
  usernameInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  avatarLabel: PropTypes.string,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  authState: 'LOGIN',
  showLogo: true,
  usernameLabel: 'Name',
  usernameInputProps: {
    type: 'text',
    placeholder: 'your username',
  },
  passwordLabel: 'Password',
  passwordInputProps: {
    type: 'password',
    placeholder: 'your password',
  },
  confirmPasswordLabel: 'Confirm Password',
  confirmPasswordInputProps: {
    type: 'password',
    placeholder: 'confirm your password',
  },
  avatarLabel: 'Avatar',
  onLogoClick: () => { },
};

export default AuthForm;
