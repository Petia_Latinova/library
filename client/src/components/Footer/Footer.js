import React from 'react';

import '../../styles/components/footer.scss';

const Footer = (props) => {

  return (
    <div className="footer section__footer">
      <div className="footer__content">
        <div className="footer__left">
          <span>Copyright &copy; 2020&nbsp;&nbsp;Aleksandrina&nbsp;&nbsp;Mankova&nbsp;&nbsp;and&nbsp;&nbsp;Petia&nbsp;&nbsp;Latinova&nbsp;</span>
        </div>
        <div className="footer__right">
          <span>Created&nbsp;&nbsp;under&nbsp;&nbsp;the&nbsp;&nbsp;MIT&nbsp;&nbsp;License</span>
        </div>
      </div>
    </div>
  );
}

export default Footer;