import React, { useState } from 'react';
import { MdSearch } from 'react-icons/md';
import { Form, Input } from 'reactstrap';

import '../../static/css/App.css';
import { withRouter} from 'react-router-dom';

const SearchInput = (props) => {
  const [text, setText] = useState('');

  const handleChange = (e) => {
    setText(e.target.value);
  }

  const hanleSearch = () => {
    props.search(text);
  }

  const keyPress = (e) => {
    if (e.keyCode === 13) {
      props.search(text);
    }
  }

  return (
    <Form inline className="cr-search-form" onSubmit={e => e.preventDefault()}>
      <div className="wrapper">
      <MdSearch
        size="20"
        className="cr-search-form__icon-search text-primary srearch-ico"
        onClick={() => hanleSearch()}
      />
      <Input
        type="search"
        className="cr-search-form__input"
        placeholder="Search and press 'Enter'"
        onChange={handleChange}
        onKeyDown={keyPress}
        value={text}
      />
      </div>
    </Form>
  );
};

export default withRouter(SearchInput);
