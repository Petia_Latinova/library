import React from 'react';
//<main className="cr-app bg-light" {...restProps}> //TODO: maybe return it later
const EmptyLayout = ({ children, ...restProps }) => (
  <main className="cr-app" {...restProps}>
    {children}
  </main>
);

export default EmptyLayout;
