import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Button } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import '../../static/css/Navigation.css';
import BookLogo from '../../static/img/login_reg_logo.png';
import Avatar from '../Profile/Avatar';
import { BASE_URL } from '../../common/constants';
import { store } from 'react-notifications-component';

const Navigation = (props) => {
  const location = props.location;
  const { isLoggedIn, user, setLoginState } = useContext(AuthContext);

  const history = props.history;

  const logout = (e) => {

    e.preventDefault();

    fetch(`${BASE_URL}/api/users/logout`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({
        isDeleted: true,
      }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })

    localStorage.removeItem('token');

    setLoginState({
      isLoggedIn: false,
      user: null,
    });

    history.push('/login');

  }

  return (
    <div>
      <Navbar>
        <Nav.Link href="/home"><img src={BookLogo} alt="Not Found" style={{ height: 40, cursor: 'pointer' }} />
          <span className="nav-library"> <b>Library</b> </span></Nav.Link>
        {!isLoggedIn && <Nav.Link href="/home">{location.pathname.includes('home') ? <b>Home</b> : "Home"}</Nav.Link>}
        {!isLoggedIn && <Nav.Link href="/about">{location.pathname.includes('about') ? <b>About</b> : "About"}</Nav.Link>}
        <Nav className="ml-auto">
          {isLoggedIn && user.role === "Admin" &&
            <Nav.Link href="/admin" className="float-right"><Button outline color="primary" className="btn-menu">Admin</Button></Nav.Link>}
          {isLoggedIn &&
            <Nav.Link className="float-right"><Button outline color="primary" className="btn-menu" onClick={logout}>Logout</Button></Nav.Link>}
          {isLoggedIn &&
            <Nav.Link href="/profile" className="float-right"><Avatar src={`${BASE_URL}/avatars/${user.avatarPath}`} size="40px" className="mb-2" /></Nav.Link>}
          {!isLoggedIn &&
            <Nav.Link href="/login" className="float-right"><Button outline color="primary" className="btn-menu">Login</Button></Nav.Link>}
          {!isLoggedIn &&
            <Nav.Link href="/signup" className="float-right"><Button outline color="primary" className="btn-menu">Sign up</Button></Nav.Link>}
        </Nav>
      </Navbar>
    </div>
  )
};

export default withRouter(Navigation);