import React from 'react';

const Avatar = ({
  rounded,
  circle,
  src,
  size,
  tag: Tag,
  className,
  style,
  ...restProps
}) => {
  return (
      <Tag
        src={src}
        style={{ width: size, height: size, ...style }}
        className={rounded ? "rounded" : "rounded-circle"}
        {...restProps}
      />
  );
};

Avatar.defaultProps = {
  tag: 'img',
  rounded: false,
  circle: true,
  size: 40,
  src: '',
  style: {},
};

export default Avatar;
