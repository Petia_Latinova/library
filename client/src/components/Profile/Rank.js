import React from 'react';

import BronzeMember from '../../static/img/bronze_member.png';
import SilverMember from '../../static/img/silver_member.png';
import GoldMember from '../../static/img/gold_member.png';

import '../../static/css/App.css';

const Rank = (props) => {

  const getRank = () => {
    if (props.points < 50) {
      return "Member";
    }
    else if (props.points < 150) {
      return <img className={props.classInput} alt="Not Found" src={BronzeMember} />;
    }
    else if (props.points < 500) {
      return <img className={props.classInput} alt="Not Found" src={SilverMember} />;
    }
    else if (props.points >= 500) {
      return <img className={props.classInput} alt="Not Found" src={GoldMember} />;
    }
  }

  return (
    <div>{getRank()}</div>
  );
}

export default Rank;
