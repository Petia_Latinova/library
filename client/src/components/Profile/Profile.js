import React, { useContext } from 'react';
import AuthContext from '../../providers/AuthContext';
import { Card, CardText, CardTitle, CardSubtitle, CardBody } from 'reactstrap';
import { BASE_URL } from '../../common/constants';
import Rank from '../Profile/Rank';
import Avatar from './Avatar';

import '../../static/css/Profile.css';

const Profile = () => {

  const { user } = useContext(AuthContext);

  return (
    <div>
      <Card inverse className="card text-white bg-gradient-theme profile-background" >
        <CardBody className="d-flex justify-content-center align-items-center flex-column">
          <span className="avatar"><Avatar src={`${BASE_URL}/avatars/${user.avatarPath}`} size="160px" className="mb-2" /></span>
          <br />
          <CardTitle className="profile-user-name">{user.username}</CardTitle>
          <CardText>
            <small>{user.readingPoints} psi</small>
          </CardText>
          <CardSubtitle className="profile-subtitle"><Rank classInput="profile-review" points={user.readingPoints} /></CardSubtitle>
        </CardBody>
      </Card>
    </div>
  );
}

export default Profile;