import React from 'react';

import '../../styles/components/carosel.scss';
import { BASE_URL } from '../../common/constants';

const Carosel = () => {

  const slides = [
    {
      title: "Subseries",
      subtitle: "Rincewind",
      description: "Adventure is never far away",
      image: `${BASE_URL}/covers/TCM.jpg`,
    },
    {
      title: "The Chronicles of Amber",
      subtitle: "Fantasy",
      description: "Galaxy Science Fiction",
      image: `${BASE_URL}/covers/Trumps_of_doom.jpg`,
    },
    {
      title: "Subseries",
      subtitle: "City Watch",
      description: "Locus Fantasy Award nominee",
      image: `${BASE_URL}/covers/TFE.jpg`,
    },
    {
      title: "What next?",
      subtitle: "Post-apocalyptic",
      description: "The planet is empty now",
      image: `${BASE_URL}/covers/Metro2035.jpg`,
    },
    {
      title: "Subseries",
      subtitle: "Überwald",
      description: "Children's fantasy novel",
      image: `${BASE_URL}/covers/TAMaurice.jpg`,
    },
  ];

  function useTilt(active) {
    const ref = React.useRef(null);

    React.useEffect(() => {
      if (!ref.current || !active) {
        return;
      }

      const state = {
        rect: undefined,
        mouseX: undefined,
        mouseY: undefined
      };

      let el = ref.current;

      const handleMouseMove = (e) => {
        if (!el) {
          return;
        }
        if (!state.rect) {
          state.rect = el.getBoundingClientRect();
        }
        state.mouseX = e.clientX;
        state.mouseY = e.clientY;
        const px = (state.mouseX - state.rect.left) / state.rect.width;
        const py = (state.mouseY - state.rect.top) / state.rect.height;

        el.style.setProperty("--px", px);
        el.style.setProperty("--py", py);
      };

      el.addEventListener("mousemove", handleMouseMove);

      return () => {
        el.removeEventListener("mousemove", handleMouseMove);
      };
    }, [active]);

    return ref;
  }

  const initialState = {
    slideIndex: 0
  };

  const slidesReducer = (state, event) => {
    if (event.type === "NEXT") {
      return {
        ...state,
        slideIndex: (state.slideIndex + 1) % slides.length
      };
    }
    if (event.type === "PREV") {
      return {
        ...state,
        slideIndex:
          state.slideIndex === 0 ? slides.length - 1 : state.slideIndex - 1
      };
    }
  };

  function Slide({ slide, offset }) {
    const active = offset === 0 ? true : null;
    const ref = useTilt(active);

    return (
      <div
        ref={ref}
        className="slide"
        data-active={active}
        style={{
          "--offset": offset,
          "--dir": offset === 0 ? 0 : offset > 0 ? 1 : -1
        }}
      >
        <div
          className="slideBackground"
          style={{
            
          }}
        />
        <div
          className="slideContent"
          style={{
            backgroundImage: `url('${slide.image}')`
          }}
        >
          <div className="slideContentInner">
            <h2 className="slideTitle">{slide.title}</h2>
            <h3 className="slideSubtitle">{slide.subtitle}</h3>
            <p className="slideDescription">{slide.description}</p>
          </div>
        </div>
      </div>
    );
  }

  const [state, dispatch] = React.useReducer(slidesReducer, initialState);

  return (
    <div className="slider-html">
      <div className="slider-body">
        <div className="slides">
          <button onClick={() => dispatch({ type: "PREV" })}>‹</button>

          {[...slides, ...slides, ...slides].map((slide, i) => {
            let offset = slides.length + (state.slideIndex - i);
            return <Slide slide={slide} offset={offset} key={i} />;
          })}
          <button onClick={() => dispatch({ type: "NEXT" })}>›</button>
        </div>
        </div>
    </div>
  );
}

export default Carosel;
