import React, { useState, useEffect, useContext } from 'react';

import '../../static/css/Reviews.css';
import { BASE_URL } from '../../common/constants';
import Review from './Review';
import ReviewEditor from './ReviewEditor';
import AuthContext from '../../providers/AuthContext';
import { Button } from 'reactstrap';
import { store } from 'react-notifications-component';

const Reviews = (props) => {
  const [appReviews, updateReviews] = useState([]);
  const [revId, setReviewIdForEdit] = useState(null);
  const [reviewTextEditor, setReviewTextEditor] = useState("");
  const [toggleEdit, setToggleEdit] = useState(false);
  const [refresh, setRefresh] = useState(false);

  const { user } = useContext(AuthContext);

  const refreshReview = () => { setRefresh(!refresh) }

  const handleEdit = (id, text) => { 
    setReviewIdForEdit(id);
    setReviewTextEditor(text);
    setToggleEdit(true);
  }

  const cleanReviewEditor = () => {
    setReviewIdForEdit(null);
    setReviewTextEditor("");
    setToggleEdit(false);
  }

  const handleToggleEdit = () => {
    setToggleEdit(true);
  }

  let adminStr = "";
  if (user.role === "Admin") {
    adminStr = "admin/"
  }

  const handleDelete = (reviewId) => {
    const result = window.confirm("Are you sure you want to delete this review?");
    if (result === false) {
      return;
    }
    fetch(`${BASE_URL}/api/${adminStr}books/${props.bookId}/reviews/${reviewId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }

        refreshReview();
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }

  useEffect(() => {
    let unmounted = false;

    fetch(`${BASE_URL}/api/books/${props.bookId}/reviews`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then(response => response.json())
      .then(result => {
        if (Array.isArray(result)) {
          if (!unmounted) {
            updateReviews(() => result);
          }
        } else {
          throw new Error(result.message);
        }
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      });

    return () => { unmounted = true };
  }, [refresh, props.bookId]);

  return (
    <div>
      {!toggleEdit && 
        <Button className="add-review" color="primary" onClick={() => handleToggleEdit()}>Add Review</Button>}
      {toggleEdit && 
        <div>
          <ReviewEditor bookId={props.bookId} reviewId={revId} cleanReviewEditor={cleanReviewEditor} 
            reviewTextEditor={reviewTextEditor} setReviewTextEditor={setReviewTextEditor} refreshReview={refreshReview} /><br/><br/>
        </div>}
        {appReviews && appReviews.map(review =>
          <Review key={review.id} reviewId={review.id} userName={review.User.name} reviewText={review.review}
            showControls={user.id === review.User.id} readingPoints={review.User.readingPoints}
            likes={review.Votes.reduce((sum, o) => sum + (o.liked ? 1 : 0), 0)}
            dislikes={review.Votes.reduce((sum, o) => sum + (o.liked ? 0 : 1), 0)}
            refreshReview={refreshReview} handleEdit={handleEdit} handleDelete={handleDelete} img={review.User.avatarPath} />)}
    </div>
  );
}

export default Reviews;