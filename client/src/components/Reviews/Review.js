import React, { useContext } from 'react';

import '../../static/css/Reviews.css';
import Votes from '../Votes/Votes';
import { Button, CardTitle, CardBody, CardText, Card, CardImg, CardFooter } from 'reactstrap';
import { BASE_URL } from '../../common/constants';
import Rank from '../Profile/Rank';
import AuthContext from '../../providers/AuthContext';

const Review = (props) => {
  const { user } = useContext(AuthContext);

  const refreshReview = () => { props.refreshReview(props.reviewId) }

  const handleEdit = () => { props.handleEdit(props.reviewId, props.reviewText) }

  const handleDelete = () => { props.handleDelete(props.reviewId) }

  return (
    < div>
      <Card className="flex-row">
        <div>
          <CardImg className="card-img-left review-avatar" src={`${BASE_URL}/avatars/${props.img}`} /><br />
          <div className="reading-points-review">{props.readingPoints} psi</div>
          <Rank classInput="rank-review" points={props.readingPoints} />
        </div>
        <CardBody className="review-body">
          <CardTitle>
            {props.userName}
          </CardTitle>
          <CardText>
            {props.reviewText}
          </CardText>
          <CardFooter className="row review-footer">
            {(props.showControls || user.role === "Admin") && <Button color="link" onClick={() => handleEdit()} >Edit</Button>}
            {(props.showControls || user.role === "Admin") && <Button color="link" onClick={() => handleDelete()}>Delete</Button>}
            <div className="review-like-group ml-auto"><Votes likes={props.likes} dislikes={props.dislikes}
              reviewId={props.reviewId} refreshReview={refreshReview} canVote={!props.showControls} />
            </div>
          </CardFooter>
        </CardBody>
      </Card>
  </div >
  );
}

export default Review;