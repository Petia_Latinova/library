import React, { useContext } from 'react';

import '../../static/css/Reviews.css';
import { FormGroup, Label, Input, Button } from 'reactstrap';
import { BASE_URL } from '../../common/constants';
import AuthContext from '../../providers/AuthContext';
import { store } from 'react-notifications-component';

const ReviewEditor = (props) => {
  const { user } = useContext(AuthContext);

  const handleTextChange = (e) => {
    props.setReviewTextEditor(e.target.value);
  }

  const CancelReview = () => {
    props.cleanReviewEditor();
  }

  const CreateUpdateReview = () => {
    let create = false;
    if (props.reviewId === null) {
      create = true;
    }

    let adminStr = "";
    if (user.role === "Admin") {
      adminStr = "admin/"
    }

    const URL = `${BASE_URL}/api/${adminStr}books/${props.bookId}/reviews`
    fetch(create ? URL : `${URL}/${props.reviewId}`, {
      method: create ? 'POST' : 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ review: props.reviewTextEditor }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return store.addNotification({
            title: 'Error',
            message: result.message,
            type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000
            }
          });
        }
        props.cleanReviewEditor();
        props.refreshReview(props.reviewId);
      })
      .catch((ex) => {
        store.addNotification({
          title: 'Error',
          message: ex.message,
          type: 'danger',                         // 'default', 'success', 'info', 'warning', 'danger'
          container: 'top-right',                // where to position the notifications
          animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
          animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
          dismiss: {
            duration: 3000
          }
        });
      })
  }

  return (
    <div>
      <FormGroup className="review-editor">
        <Label for="exampleText">{props.reviewId !== null ? "Edit Review" : "Create Review"}</Label>
        <Input type="textarea" name="text" onChange={handleTextChange} value={props.reviewTextEditor} />
        <Button className="cansel-review float-right" color="primary" onClick={() => CancelReview()}>Cancel</Button>
        <Button className="save-review float-right" color="primary" onClick={() => CreateUpdateReview()}>Save</Button>
      </FormGroup>     
    </div>
  );
}

export default ReviewEditor;