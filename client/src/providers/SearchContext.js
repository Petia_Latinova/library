import { createContext } from 'react';

const SearchContext = createContext({
  keyword: '',
  query: '',
  setKeyword: () => {},
  setQuery: () => { },
});

export default SearchContext;
