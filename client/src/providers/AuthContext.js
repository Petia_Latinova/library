import { createContext } from "react";
import JwtDecode from "jwt-decode";

const AuthContext = createContext({
  isLoggedIn: false, // true or false - is the user logged in
  user: null, // null or the logged user's payload
  setLoginState: () => { },
});

export const getToken = () => {
  return localStorage.getItem('token');
};

export const extractUser = token => {
  try {
    return JwtDecode(token || '');
  } catch (e) {
    localStorage.removeItem('token');
    return null;
  }
}

export default AuthContext;