import React, { useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import './static/css/App.css';
import Home from './components/Home/Home';
import About from './components/About/About';
import Books from './components/Books/Books';
import BookDetails from './components/Books/BookDetails';
import Profile from './components/Profile/Profile';
import Footer from './components/Footer/Footer';
import Navigation from './components/Navigation/Navigation';
import './styles/reduction.scss';
import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import { STATE_LOGIN, STATE_SIGNUP } from './components/Login/AuthForm';
import LayoutRoute from './components/Layout/LayoutRoute';
import EmptyLayout from './components/Layout/EmptyLayout';
import Login from './components/Login/Login';
import Admin from './components/Admin/Admin';
import NotFound from './components/NotFound/NotFound';
import ReactNotifications from 'react-notifications-component';

import 'react-notifications-component/dist/theme.css';
import 'animate.css';

function App() {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken())
  });

  return (
    <BrowserRouter>
      <ReactNotifications />
      <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
        <Container fluid>
          <Navigation />
          <Row className="main_background">
            <Col>
              <Switch>
                <Redirect path="/" exact to="/home" />
                <Route path="/home" component={Home}>
                  {authValue.isLoggedIn && <Redirect to="/books" />}
                </Route>
                <Route path="/about" component={About} >
                  {authValue.isLoggedIn && <Redirect to="/books" />}
                </Route>
                <Route path="/books" exact component={Books}>
                  {!authValue.isLoggedIn && <Redirect to="/login" />}
                </Route>
                <Route path="/books/:id" component={BookDetails} >
                  {!authValue.isLoggedIn && <Redirect to="/login" />}
                </Route>
                <Route path="/profile" exact component={Profile} >
                  {!authValue.isLoggedIn && <Redirect to="/login" />}
                </Route>
                <Route path="/profile/:name" component={Profile}>
                  {!authValue.isLoggedIn && <Redirect to="/login" />}
                </Route>
                <Route path="/admin" component={Admin}>
                  {!authValue.isLoggedIn ? <Redirect to="/login" /> :
                    authValue.user && authValue.user.role !== 'Admin' && <Redirect to="/error" />}
                </Route>
                <LayoutRoute
                  exact
                  path="/login"
                  layout={EmptyLayout}
                  component={props => (
                    <Login {...props} authState={STATE_LOGIN} />
                  )}
                />
                <LayoutRoute
                  exact
                  path="/signup"
                  layout={EmptyLayout}
                  component={props => (
                    <Login {...props} authState={STATE_SIGNUP} />
                  )}
                />
                <Route path="*" component={NotFound} />
              </Switch>
            </Col>
          </Row>
          <Row>
            <Footer />
          </Row>
        </Container>
      </AuthContext.Provider>
    </BrowserRouter >
  );
}

export default App;
