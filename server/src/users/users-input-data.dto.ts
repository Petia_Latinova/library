import { IsString, IsNotEmpty, Length, Matches } from 'class-validator';


export class UsersInputDataDTO {
  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  name: string;

  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/, {
    message:
      'Must contain at least one number, one uppercase and lowercase letter, one spacial character and from 8 to 15 characters',
  })
  password: string;
}
