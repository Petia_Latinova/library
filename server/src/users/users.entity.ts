import { Books } from './../books/books.entity';
import { Votes } from './../votes/vote.entity';
import { Reviews } from './../reviews/reviews.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Rate } from './../rate/rate.entity';
import { UserRole } from './passport/users-role';

@Entity('users')
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('nvarchar', { length: 25 })
  name: string;

  @Column('nvarchar', { length: 256 })
  password: string;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.Basic,
  })
  role: UserRole;

  @Column('int', { default: 0 })
  readingPoints: number;

  @OneToMany(
    () => Books,

    book => book.user,
  )
  books: Books[];

  @OneToMany(
    () => Rate,
    rate => rate.user,
  )
  rates: Rate[];

  @OneToMany(
    () => Reviews,
    review => review.user,
  )
  reviews: Reviews[];

  @OneToMany(
    () => Votes,
    vote => vote.user,
  )
  votes: Votes[];

  @Column('nvarchar', { length: 100, default: "noAvatar.jpg" })
  avatarPath: string;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;
}
