import {
  Injectable, NotFoundException, ForbiddenException, UnauthorizedException } from '@nestjs/common';
import { Users } from './users.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersInputDataDTO } from './users-input-data.dto';
import { UsersDTO } from './users.dto';
import * as bcrypt from 'bcrypt';
import { UsersTransformService } from './users-transform.service';
import { JwtService } from '@nestjs/jwt';
import { JWTPayload } from './passport/jwt-payload';
import { UserRole } from './passport/users-role';
import { Token } from './token.entity';

@Injectable()
export class UsersService {
  private static readonly USER_DUPLICATED_ERROR = `User already exist`;
  private static readonly USER_DOESNT_EXIST_ERROR = `User doesn't exist`;
  private static readonly USER_NOT_FOUND_ERROR = `No user found`;
  private static readonly USER_WRONG_CREDENTIALS_ERROR = `Wrong credentials!`;

  constructor(
    @InjectRepository(Users) private readonly usersRepository: Repository<Users>,
    @InjectRepository(Token) private readonly tokenRepository: Repository<Token>,
    private readonly userTransformService: UsersTransformService,
    private readonly jwtService: JwtService,
  ) {}

  async register(userInput: UsersInputDataDTO): Promise<UsersDTO> {
    await this.doesUserExist(userInput.name);
    const user = this.usersRepository.create(userInput);
    // hash the user's password
    user.password = await bcrypt.hash(user.password, 10);

    const created = await this.usersRepository.save(user);

    return this.userTransformService.toUserDTO(created);
  }

  public async login(loginUser: UsersInputDataDTO): Promise<string> {
    const user = await this.validateUser(loginUser.name, loginUser.password);

    if (!user) {
      throw new UnauthorizedException(UsersService.USER_WRONG_CREDENTIALS_ERROR);
    }

    const payload: JWTPayload = {
      id: user.id,
      username: user.name,
      readingPoints: user.readingPoints,
      avatarPath: user.avatarPath,
      role: UserRole[user.role],
    };

    const token = await this.jwtService.signAsync(payload);

    return token; // this will be returned to the client on successful login
  }

  public async validateUser( username: string, password: string ): Promise<Users> {
    const user = await this.findUserByName(username);
    if (!user) {
      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);
    return isUserValidated ? user : null;
  }

  public async doesUserExist(userName: string): Promise<Users> {
    const foundUser: Users = await this.usersRepository.findOne({ name: userName });
    if (foundUser !== undefined) {
      throw new ForbiddenException(UsersService.USER_DUPLICATED_ERROR);
    }
    return foundUser;
  }

  public async findUserByName(userName: string): Promise<Users> {
    const foundUser: Users = await this.usersRepository.findOne({ name: userName, isDeleted: false });
    if (foundUser === undefined) {
      throw new ForbiddenException(UsersService.USER_DOESNT_EXIST_ERROR);
    }
    return foundUser;
  }

  public async findUserById(userId: number): Promise<Users> {
    const foundUser: Users = await this.usersRepository.findOne({ id: userId });
    if (foundUser === undefined) {
      throw new NotFoundException(UsersService.USER_NOT_FOUND_ERROR);
    }
    return foundUser;
  }

  public async updateReadingPoints( userId: number, points: number ): Promise<void> {
    const user = await this.findUserById(userId);
    user.readingPoints += points;

    await this.usersRepository.save(user);
  }

  async isBlacklisted(token: string): Promise<boolean> {
    return Boolean(await this.tokenRepository.findOne({
      where: {
        token,
      }
    }));
  }

  async blacklist(token: string): Promise<void> {
    const tokenEntity = this.tokenRepository.create();
    tokenEntity.token = token;

    await this.tokenRepository.save(tokenEntity)
  }

  async uploadAvatar(id: number, filename: string) {
    const user = await this.findUserById(id);

    user.avatarPath = filename;
    await this.usersRepository.save(user);

    return this.userTransformService.toUserDTO(user);
  }

}
