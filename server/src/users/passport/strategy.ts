import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { UsersService } from "../users.service";
import { ExtractJwt, Strategy } from "passport-jwt";
import { jwtConstants } from "./secret";
import { JWTPayload } from "./jwt-payload";
import { Users } from "../users.entity";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  
  constructor(private readonly usersService: UsersService ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: JWTPayload): Promise<Users> {
    // the returned user is injected into Request
    return await this.usersService.findUserByName(payload.username);
  }
}