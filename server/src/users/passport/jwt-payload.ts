export class JWTPayload {
  id: number;
  username: string;
  readingPoints: number;
  avatarPath: string;
  role: string; // map UserRole to string
}