import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Request } from 'express';
import { UserRole } from './users-role';
import { Users } from '../users.entity';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        role: UserRole,
    ) {
        this.role = role;
    }

    private role: UserRole;


    canActivate(context: ExecutionContext): boolean {
        const request = context.switchToHttp().getRequest<Request>();
        const user = request.user as Users;

        return user?.role === this.role;
    }
}

