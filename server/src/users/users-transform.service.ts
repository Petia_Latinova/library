import { Users } from './users.entity';
import { UsersDTO } from './users.dto';

export class UsersTransformService {
  toUserDTO(user: Users): UsersDTO {
    return {
      id: user.id,
      name: user.name,
      readingPoints: user.readingPoints,
      avatarPath: user.avatarPath,
    };
  }
}
