import { UsersDTO } from './users.dto';
import {
  Controller,
  Post,
  Body,
  BadRequestException,
  ValidationPipe,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Param,
} from '@nestjs/common';
import { UsersInputDataDTO } from './users-input-data.dto';
import { UsersService } from './users.service';
import { GetToken } from './passport/get-token.decorator';
import { BlacklistGuard } from './passport/blacklist.guard';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('login')
  async login(
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    user: UsersInputDataDTO,
  ): Promise<{ token: string }> {
    const token = await this.usersService.login(user);
    if (!token) {
      throw new BadRequestException(`Invalid email and/or password!`);
    }

    return { token };
  }

  @Post()
  async register(
    @Body(new ValidationPipe({ whitelist: true }))
    usersInput: UsersInputDataDTO,
  ): Promise<UsersDTO> {
    return await this.usersService.register(usersInput);
  }

  @Delete('logout')
  @UseGuards(BlacklistGuard, AuthGuard('jwt'))
  async logout(@GetToken() token: string): Promise<{ message: string }> {
    await this.usersService.blacklist(token?.slice(7));

    return {
      message: 'You have been logged out!',
    };
  }

  @Post(':id/avatar')
  @UseInterceptors(
    FileInterceptor('files', {
      storage: diskStorage({
        destination: './images/avatars',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          //Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadAvatar(@UploadedFile() files, @Param('id') id: string) {
    return await this.usersService.uploadAvatar(Number(id), files.filename);
  }
}
