export class UsersDTO {
  id: number;
  name: string;
  readingPoints: number;
  avatarPath: string;
}
