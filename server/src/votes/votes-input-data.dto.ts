import { IsBoolean } from "class-validator";

export class VotesInputDataDTO {
  @IsBoolean()
  liked: boolean;
}