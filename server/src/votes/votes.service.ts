import { Injectable, NotFoundException } from '@nestjs/common';
import { VotesInputDataDTO } from './votes-input-data.dto';
import { VotesDTO } from './votes.dto';
import { Votes } from './vote.entity';
import { ReviewsService } from './../reviews/reviews.service';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { VotesTransformService } from './votes-transform.service';
import { AdminUsersService } from './../admin-users/admin-users.service';
import { UsersService } from './../users/users.service';

@Injectable()
export class VotesService {
  private static readonly LIKE_REVIEW_READING_POINTS = 1;
  private static readonly LIKE_NOT_FOUND_ERROR = `Like not found`;

  constructor(
    @InjectRepository(Votes) private readonly votesRepository: Repository<Votes>,
    private readonly reviewsService: ReviewsService,
    private readonly votesTransformService: VotesTransformService,
    private readonly adminUsersService: AdminUsersService,
    private readonly usersService: UsersService,
  ) {}

  public async likeReviews(id: number, votesInput: VotesInputDataDTO, userId: number ): Promise<VotesDTO> {
    await this.adminUsersService.doesUserBanned(userId);
    const review = await this.reviewsService.findReviewById(id); //check does review exist
    let vote: Votes;

    try { //update vote
      vote = await this.findVote(userId, id); // try to get vote from DB
      vote.liked = votesInput.liked; // update the existing vote
    } catch (e) { //create new vote
      vote = this.votesRepository.create(votesInput);
      vote.reviewId = id;
      vote.userId = userId;
    }

    const createdVote = await this.votesRepository.save(vote);

    if (vote.liked) {
      await this.usersService.updateReadingPoints( review.userId, VotesService.LIKE_REVIEW_READING_POINTS );
    }

    return this.votesTransformService.toVoteDTO( await this.votesRepository.findOne({
      relations: ['user'],
      where: { id: createdVote.id }
    }));
  }

  public async findVote(userId: number, reviewId: number): Promise<Votes> {
    const vote: Votes = await this.votesRepository.findOne({ userId: userId, reviewId: reviewId });
    if (vote === undefined) {
      throw new NotFoundException(VotesService.LIKE_NOT_FOUND_ERROR);
    }
    return vote;
  }
}
