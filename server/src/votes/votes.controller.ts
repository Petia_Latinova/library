import { Controller, Put, Param, Body, UseGuards, ValidationPipe } from '@nestjs/common';
import { VotesDTO } from './votes.dto';
import { VotesService } from './votes.service';
import { VotesInputDataDTO } from './votes-input-data.dto';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from './../users/passport/blacklist.guard';
import { UserId } from './../users/passport/user-id.decorator';

@Controller('reviews/:id/votes')
  @UseGuards(BlacklistGuard, AuthGuard('jwt'))
export class VotesController {
  constructor(private readonly votesService: VotesService) {}

  @Put()
  public async likeReviews(@Param('id') bookId: number, @Body(new ValidationPipe({ whitelist: true })) votes: VotesInputDataDTO,
    @UserId() userId: number): Promise<VotesDTO> {
      return await this.votesService.likeReviews(bookId, votes, userId);
  }

}
