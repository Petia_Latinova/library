import { UsersDTO } from "./../users/users.dto";

export class VotesDTO {
  id: number;
  User: UsersDTO;
  liked: boolean;
}
