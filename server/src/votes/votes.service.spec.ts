import { VotesService } from './votes.service';
import { Reviews } from './../reviews/reviews.entity';

describe('ReviewsService', () => {

  it('should initialize', () => {

    const service = new VotesService(null, null, null, null, null);

    expect(service).toBeDefined();
  });

  it('likeReviews', () => {

    const votesRepositoryMock = {
      async findOne() {
        return [{
          "id": 1,
          "User": {
            "id": 1,
            "name": "User1"
          },
          "liked": true
        }];
      },
      async save() {
        return [{
          "id": 1,
          "User": {
            "id": 1,
            "name": "User1"
          },
          "liked": true
        }];
      }
    };

    const VotesTransformServiceMock = {
      toVoteDTO() { return null }
    }

    const adminUsersServiceMock = {
      async doesUserBanned() { return null }
    }

    const reviewsServiceMock = {
      async findReviewById(): Promise<Reviews> { 
        const review: Reviews = new Reviews();
        review.userId = 1;
        return review;
      }
    }

    const usersServiceMock = {
      async updateReadingPoints() {
        return null;
      }
    }

    const service = new VotesService(votesRepositoryMock as any, reviewsServiceMock as any,
      VotesTransformServiceMock as any, adminUsersServiceMock as any, usersServiceMock as any);
    jest.spyOn(votesRepositoryMock, 'findOne');

    service.likeReviews(1, { "liked": true }, 1);

    expect(votesRepositoryMock.findOne).toHaveBeenCalledWith({
      relations: ['user'],
      where: { id: 1 }
    });
  });

  


});
