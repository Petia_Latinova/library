import { Reviews } from './../reviews/reviews.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
} from 'typeorm';
import { Users } from './../users/users.entity';

@Entity('votes')
export class Votes {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'boolean', default: false })
  liked: boolean;

  @ManyToOne(() => Reviews)
  review: Reviews;

  @Column({ nullable: true })
  reviewId: number;

  @ManyToOne(() => Users)
  user: Users;

  @Column({ nullable: true })
  userId: number;
}
