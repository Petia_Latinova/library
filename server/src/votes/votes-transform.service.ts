import { Votes } from "./vote.entity";
import { VotesDTO } from "./votes.dto";
import { UsersTransformService } from "./../users/users-transform.service";
import { Inject } from "@nestjs/common";

export class VotesTransformService {

  constructor(@Inject(UsersTransformService) private readonly userTransformService: UsersTransformService) { }

  toVoteDTO(vote: Votes): VotesDTO {
    return {
      id: vote.id,
      User: this.userTransformService.toUserDTO(vote.user),
      liked: vote.liked,
    }
  }

}
