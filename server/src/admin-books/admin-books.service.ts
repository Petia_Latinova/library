import { BooksTransformService } from './../books/books-transform.service';
import { BooksService } from './../books/books.service';
import { Books } from './../books/books.entity';
import { AdminUsersService } from './../admin-users/admin-users.service';
import { BookInputDTO } from './input-book.dto';
import { Injectable } from '@nestjs/common';
import { BooksDTO } from './../books/books.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AdminBooksService {
  
  constructor(
    private readonly adminUsersService: AdminUsersService,
    @InjectRepository(Books)
    private readonly booksRepository: Repository<Books>,

    private readonly bookService: BooksService,

    private readonly booksTransformService: BooksTransformService,
  ) {}

  public async createBook(book: BookInputDTO, userId: number): Promise<BooksDTO> {
    await this.adminUsersService.doesUserBanned(userId);

    const createBook: Books = this.booksRepository.create(book);
    createBook.userId = userId;
    const createdBook = await this.booksRepository.save(createBook);

    return this.booksTransformService.toBooksDTO(
      await this.booksRepository.findOne({
        relations: ['rates', 'rates.user'],
        where: { id: createdBook.id },
      }),
    );
  }

  public async updateBook(
    bookId: number,
    book: BookInputDTO,
    userId: number,
  ): Promise<BooksDTO> {
    await this.adminUsersService.doesUserBanned(userId);

    const oldBook: BooksDTO = await this.bookService.getById(bookId);
    oldBook.author = book.author;
    oldBook.title = book.title;

    await this.booksRepository.save(oldBook);

    return this.booksTransformService.toBooksDTO(
      await this.booksRepository.findOne({
        relations: ['rates', 'rates.user'],
        where: { id: bookId, isDelisted: false },
      }),
    );
  }

  public async deleteBook(
    bookId: number,
    userId: number,
  ): Promise<{ msg: string }> {
    await this.adminUsersService.doesUserBanned(userId);
    const bookForDelete: Books = await this.bookService.findBookById(bookId);

    bookForDelete.isDelisted = true;

    await this.booksRepository.save(bookForDelete);

    await this.booksRepository
      .createQueryBuilder()
      .update(Books)
      .where('id = :id', { id: bookId })
      .set({ isDelisted: true })  
      .execute();

    return {
      msg: 'Book has been delisted!',
    };
  }

  async uploadCover(id: number, filename: string) {
    const book = await this.bookService.findBookById(id);
    book.photoPath = filename;
    await this.booksRepository.save(book);
    return this.booksTransformService.toBooksDTO(book);
  }

}
