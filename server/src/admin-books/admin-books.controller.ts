import { AdminBooksService } from './admin-books.service';
import { BookInputDTO } from './input-book.dto';
import { BooksService } from './../books/books.service';
import {
  Controller,
  UseGuards,
  Get,
  Param,
  NotFoundException,
  Post,
  Put,
  Delete,
  Body,
  ValidationPipe,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from './../users/passport/roles.guard';
import { UserRole } from './../users/passport/users-role';
import { BooksDTO } from './../books/books.dto';
import { BlacklistGuard } from './../users/passport/blacklist.guard';
import { UserId } from './../users/passport/user-id.decorator';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Controller('admin/books')
@UseGuards(BlacklistGuard, AuthGuard('jwt'), new RolesGuard(UserRole.Admin))
export class AdminBooksController {
  
  constructor(
    private readonly adminBooksService: AdminBooksService,
    private readonly booksService: BooksService,
  ) {}

  @Get(':id')
  public async getById(@Param('id') id: number): Promise<BooksDTO> {
    const book = await this.booksService.getById(id);

    if (book === undefined) {
      throw new NotFoundException(`No book with id ${id}`);
    }

    return book;
  }

  @Post()
  public async createBook(@Body(new ValidationPipe({ whitelist: true })) book: BookInputDTO,
    @UserId() userId: number): Promise<BooksDTO> {
      return await this.adminBooksService.createBook(book, userId);
  }

  @Put(':id')
  public async updateBook(
    @Param('id') bookId: number,
    @Body(new ValidationPipe({ whitelist: true })) book: BookInputDTO,
    @UserId() userId: number,
  ): Promise<BooksDTO> {
    return await this.adminBooksService.updateBook(bookId, book, userId);
  }

  @Delete(':id')
  public async deleteBook(
    @Param('id') bookId: number,
    @UserId() userId: number,
  ): Promise<{ msg: string }> {
    return await this.adminBooksService.deleteBook(bookId, userId);
  }

  @Post(':id/cover')
  @UseInterceptors(
    FileInterceptor('files', {
      storage: diskStorage({
        destination: './images/covers',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          //Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadCover(@UploadedFile() files, @Param('id') id: string) {
    return await this.adminBooksService.uploadCover(Number(id), files.filename);
  }
}
