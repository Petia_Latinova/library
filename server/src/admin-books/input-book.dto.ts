import { IsString, IsNotEmpty, Length, IsUrl, IsOptional } from "class-validator";

export class BookInputDTO {
  @IsString()
  @IsNotEmpty()
  @Length(2, 50)
  title: string;

  @IsString()
  @IsNotEmpty()
  @Length(2, 1000)
  author: string;
}
