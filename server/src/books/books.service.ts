import { UsersService } from './../users/users.service';
import { BorrowHistory } from './borrow-history.entity';
import { BooksTransformService } from './books-transform.service';
import { Books } from './books.entity';
import { BooksDTO } from './books.dto';
import {
  Injectable,
  NotFoundException,
  ForbiddenException,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BooksService {

  private static readonly READING_POINTS = 10;

  private static readonly BOOK_IS_NOT_FOUND_ERROR = `No book is found`;
  private static readonly BOOK_IS_BORROWED_ERROR = `Book is borrowed!`;
  private static readonly BOOK_CANT_RETURN_ERROR =  `Can't return book that you didn't took`;
  private static readonly BOOK_CANT_BE_RATED_ERROR = `This user can't rate book or add review`;


  constructor(
    @InjectRepository(Books)
    private readonly booksRepository: Repository<Books>,

    @InjectRepository(BorrowHistory)
    private readonly borrowHistoryRepository: Repository<BorrowHistory>,

    private readonly booksTransformService: BooksTransformService,

    private readonly userService: UsersService,
  ) {}

  public async getAllBooks(search: string, page: string, limit: string): Promise<BooksDTO[]> {
    const qb = await this.booksRepository.createQueryBuilder("books")
      .leftJoinAndSelect('books.rates', 'rates')
      .leftJoinAndSelect('rates.user', 'user')
      .where('books.isDelisted = false');

    if (search) {
      qb.andWhere(`(LOWER(books.author) LIKE LOWER(:search) OR LOWER(books.title) LIKE LOWER(:search))`, { search: `%${search}%` });
    }

    if(page && limit) {
      qb.skip(Number(page) * Number(limit)).take(Number(limit));
    }

    const books = (await qb.orderBy("books.id").getMany());

    return books.map(e => this.booksTransformService.toBooksDTO(e));
  }

  public async findBookById(id: number): Promise<Books> {
    const book = await this.booksRepository.findOne({
      relations: ['rates', 'rates.user'],
      where: { id: id, isDelisted: false },
    });

    if (!book) {
      throw new NotFoundException(BooksService.BOOK_IS_NOT_FOUND_ERROR);
    } else {
      return book;
    }
  }

  async getById(id: number): Promise<BooksDTO> {
    return this.booksTransformService.toBooksDTO(await this.findBookById(id));
  }

  async isBorrowed(id: number, userId: number): Promise<BooksDTO> {
    const bookIsTaken = await this.findBookById(id);

    if (bookIsTaken.isBorrowed === true) {
      throw new ForbiddenException(BooksService.BOOK_IS_BORROWED_ERROR);
    }
    bookIsTaken.userId = userId;
    bookIsTaken.isBorrowed = true;

    const borrowedBook = await this.booksRepository.save(bookIsTaken);

    return this.booksTransformService.toBooksDTO(borrowedBook);
  }

  public async returnBook(id: number, userId: number): Promise<BooksDTO> {
    const bookToReturn = await this.findBookById(id);

    if (bookToReturn.isBorrowed === true && bookToReturn.userId !== userId) {
      throw new ForbiddenException(BooksService.BOOK_CANT_RETURN_ERROR);
    }

    bookToReturn.userId = userId;
    bookToReturn.isBorrowed = false;

    const returnedBook = await this.booksRepository.save(bookToReturn);

    const history: BorrowHistory = this.borrowHistoryRepository.create();
    history.userId = userId;
    history.bookId = id;
    await this.borrowHistoryRepository.save(history);

    await this.userService.updateReadingPoints(userId, BooksService.READING_POINTS);

    return this.booksTransformService.toBooksDTO(returnedBook);
  }

  public async borrowHistory(
    userId: number,
    bookId: number,
  ): Promise<BorrowHistory> {
    const history: BorrowHistory = await this.borrowHistoryRepository.findOne({
      userId: userId,
      bookId: bookId,
    });
    if (history === undefined) {
      throw new NotFoundException(BooksService.BOOK_CANT_BE_RATED_ERROR);
    }
    return history;
  }

}
