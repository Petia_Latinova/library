import { Reviews } from './../reviews/reviews.entity';
import { Rate } from './../rate/rate.entity';
import { Users } from './../users/users.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity('books')
export class Books {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('nvarchar', { length: 1000 })
  title: string;

  @Column('nvarchar', { length: 50 })
  author: string;

  @Column({ type: 'boolean', default: false })
  isDelisted: boolean;

  @Column({ type: 'boolean', default: false })
  isBorrowed: boolean;

  @ManyToOne(
    () => Users,
    user => user.id,
  )
  user: Users;

  @Column({ nullable: true })
  userId: number;

  @OneToMany(
    () => Rate,
    rate => rate.book,
  )
  rates: Rate[];

  @OneToMany(
    () => Reviews,
    review => review.book,
  )
  reviews: Reviews[];

  @Column('nvarchar', { length: 100, default: "noCover.jpg" })
  photoPath: string;

}
