import { BooksService } from './books.service';
import { BooksDTO } from './books.dto';
import {
  Controller,
  Get,
  Param,
  NotFoundException,
  Post,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from './../users/passport/blacklist.guard';
import { UserId } from './../users/passport/user-id.decorator';

@Controller('books')
  @UseGuards(BlacklistGuard, AuthGuard('jwt'))
export class BooksController {
  constructor(private readonly booksService: BooksService) {}

  @Get()
  public async allBooks(
    @Query('search') search: string,
    @Query('page') page: string,
    @Query('limit') limit: string,
  ): Promise<BooksDTO[]> {
    const books = await this.booksService.getAllBooks(search, page, limit);
    return books;
  }

  @Get(':id')
  public async getById(@Param('id') id: number): Promise<BooksDTO> {
    const book = await this.booksService.getById(id);

    if (book === undefined) {
      throw new NotFoundException(`No book with id ${id}`);
    }

    return book;
  }

  @Post(':id')
  async isBorrowed(
    @Param('id')
    bookId: number,
    @UserId() userId: number,
  ): Promise<BooksDTO> {
    return await this.booksService.isBorrowed(bookId, userId);
  }

  @Delete(':id')
  async returnBook(
    @Param('id') id: number,
    @UserId() userId: number,
  ): Promise<BooksDTO> {
    return await this.booksService.returnBook(id, userId);
  }

}
