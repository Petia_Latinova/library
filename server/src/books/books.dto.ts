import { RateDTO } from './../rate/rate.dto';

export class BooksDTO {
  id: number;
  title: string;
  author: string;
  isBorrowed: boolean;
  Rate: RateDTO[];
  photoPath: string;
  userId: number;
}
