import { RateTransformService } from './../rate/rate-transform.service';
import { BooksDTO } from './books.dto';
import { Inject } from '@nestjs/common';
import { Books } from './books.entity';
export class BooksTransformService {
  constructor(
    @Inject(RateTransformService)
    private readonly rateTransformService: RateTransformService,
  ) {}
  toBooksDTO(book: Books): BooksDTO {
    return {
      id: book.id,
      title: book.title,
      author: book.author,
      isBorrowed: book.isBorrowed,
      Rate: book.rates.map(e => this.rateTransformService.toRateDTO(e)),
      photoPath: book.photoPath,
      userId: book.userId,
    };
  }
}
