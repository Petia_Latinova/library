import {
  PrimaryGeneratedColumn,
  Entity,
  Column,
  ManyToOne,
  CreateDateColumn,
} from 'typeorm';
import { Books } from './books.entity';
import { Users } from './../users/users.entity';

@Entity('borrowhistory')
export class BorrowHistory {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Books)
  book: Books;

  @Column({ nullable: true })
  bookId: number;

  @ManyToOne(() => Users)
  user: Users;

  @Column({ nullable: true })
  userId: number;

  @CreateDateColumn({ default: null })
  date: Date;
}
