import * as bcrypt from 'bcrypt';
import { Users } from './../users/users.entity';
import { Repository, createConnection } from 'typeorm';
import { UserRole } from './../users/passport/users-role';
import { Books } from './../books/books.entity';
import { Reviews } from './../reviews/reviews.entity';
import { Rate } from './../rate/rate.entity';

const seedsUsers = async (connection: any) => {
  // SENSITIVE DATA ALERT! - Normally the seeds and the admin credentials should not be present in the public repository!
  // Run: `npm run seeds` to seed the database

  //insert in users
  const usersRepository: Repository<Users> = connection.manager.getRepository(Users);

  const users = [
    {
      name: 'admin',
      password: await bcrypt.hash('123456aA!', 10),
      role: UserRole.Admin,
      readingPoints: 1000,
      avatarPath: 'adminAvatar.jpg',
    },
    {
      name: 'basic',
      password: await bcrypt.hash('123456aA!', 10),
      role: UserRole.Basic,
    },
    {
      name: 'Aleksandrina',
      password: await bcrypt.hash('123456aA!', 10),
      role: UserRole.Admin,
      readingPoints: 2000,
    },
    {
      name: 'Petia',
      password: await bcrypt.hash('123456aA!', 10),
      role: UserRole.Basic,
      readingPoints: 160,
      avatarPath: 'PetiaAvatar.jpg',
    },
  ];

  for (const user of users) {
    const foundUser = await usersRepository.findOne({
      name: user.name,
    });

    if (!foundUser) {
      const userToCreate = usersRepository.create(user);
      console.log(await usersRepository.save(userToCreate));
    }
  }
};

const seedsBooks = async (connection: any) => {
  //insert in books
  const booksRepository: Repository<Books> = connection.manager.getRepository(Books);

  const books = [{
    title: 'The Colour of Magic',
    author: 'Terry Pratchett',
    photoPath: 'TCM.jpg',
  },
  {
    title: 'The light fantastic',
    author: 'Terry Pratchett',
    isBorrowed: true,
    userId: 1,
    photoPath: 'TLF.jpg',
  },
  {
    title: 'Equal Rites',
    author: 'Terry Pratchett',
    isBorrowed: true,
    userId: 1,
    photoPath: 'EqR.jpg',
  },
  {
  title: 'Mort',
  author: 'Terry Pratchett',
  },
  {
    title: 'Sourcery',
    author: 'Terry Pratchett',
    photoPath: 'S.jpg',
  },
  {
    title: 'Wyrd Sisters',
    author: 'Terry Pratchett',
    isBorrowed: true,
    userId: 2,
    photoPath: 'WS.jpg',
  },
  {
    title: 'Pyramids',
    author: 'Terry Pratchett',
    photoPath: 'Py.jpg',
  },
  {
    title: 'Guards! Guards!',
    author: 'Terry Pratchett',
    photoPath: 'GG.jpg',
  },
  {
    title: 'Eric',
    author: 'Terry Pratchett',
    photoPath: 'Er.jpg',
  },
  {
    title: 'Moving Pictures',
    author: 'Terry Pratchett',
    photoPath: 'MP.jpg',
  },
  {
    title: 'Reaper Man',
    author: 'Terry Pratchett',
  },
  {
    title: 'Witches Abroad',
    author: 'Terry Pratchett',
    photoPath: 'WA.jpg',
  },
  {
    title: 'Small Gods',
    author: 'Terry Pratchett',
  },
  {
    title: 'Lords and Ladies',
    author: 'Terry Pratchett',
    photoPath: 'LandL.jpg',
  },
  {
    title: 'Men at Arms',
    author: 'Terry Pratchett',
    photoPath: 'MA.jpg',
  },
  {
    title: 'Soul Music',
    author: 'Terry Pratchett',
    isBorrowed: true,
    userId: 2,
    photoPath: 'SM.jpg',
  },
  {
    title: 'Interesting Times',
    author: 'Terry Pratchett',
    photoPath: 'IT.jpg',
  },
  {
    title: 'Maskerade',
    author: 'Terry Pratchett',
    photoPath: 'Maskerade.jpg',
  },
  {
    title: 'Feet of Clay',
    author: 'Terry Pratchett',
    isBorrowed: true,
    userId: 3,
    photoPath: 'FC.jpg',
  },
  {
    title: 'Hogfather',
    author: 'Terry Pratchett',
    photoPath: 'Hogfather.jpg',
  },
  {
    title: 'Jingo',
    author: 'Terry Pratchett',
    photoPath: 'Jingo.jpg',
  },
  {
    title: 'The Last Continent',
    author: 'Terry Pratchett',
    photoPath: 'TLC.jpg',
  },
  {
    title: 'Carpe Jugulum',
    author: 'Terry Pratchett',
    photoPath: 'CJ.jpg',
  },
  {
    title: 'The fifth elephant',
    author: 'Terry Pratchett',
    photoPath: 'TFE.jpg',
  },
  {
    title: 'The Truth',
    author: 'Terry Pratchett',
    isBorrowed: true,
    userId: 3,
    photoPath: 'TTruth.jpg',
  },
  {
    title: 'Thief of Time',
    author: 'Terry Pratchett',
    photoPath: 'ThiefTime.jpg',
  },
  {
    title: 'The fifth elephant',
    author: 'Terry Pratchett',
    photoPath: 'TFE.jpg',
  },
  {
    title: 'The Last Hero',
    author: 'Terry Pratchett',
    photoPath: 'TheLH.jpg',
  },
  {
    title: 'The Amazing Maurice and His Educated Rodents',
    author: 'Terry Pratchett',
    photoPath: 'TAMaurice.jpg',
  },
  {
    title: 'Night Watch',
    author: 'Terry Pratchett',
    photoPath: 'NightWatch.jpg',
  },
  {
    title: 'The Wee Free Men',
    author: 'Terry Pratchett',
    photoPath: 'WFM.jpg',
  },
  {
    title: 'Monstrous Regiment',
    author: 'Terry Pratchett',
    photoPath: 'MR.jpg',
  },
  {
    title: 'A Hat Full of Sky',
    author: 'Terry Pratchett',
    photoPath: 'HatFullSky.jpg',
  },
  {
    title: 'Going Postal',
    author: 'Terry Pratchett',
    photoPath: 'GoingPostal.jpg',
  },
  {
    title: 'Thud!',
    author: 'Terry Pratchett',
    photoPath: 'Thud.jpg',
  },
  {
    title: 'Wintersmith',
    author: 'Terry Pratchett',
    photoPath: 'Wintersmith.jpg',
  },
  {
    title: 'Making Money',
    author: 'Terry Pratchett',
    photoPath: 'MakingMoney.jpg',
  },
  {
    title: 'Unseen Academicals',
    author: 'Terry Pratchett',
    isBorrowed: true,
    userId: 4,
    photoPath: 'UnseenAcademicals.jpg',
  },
  {
    title: 'I Shall Wear Midnight',
    author: 'Terry Pratchett',
    isBorrowed: true,
    userId: 4,
    photoPath: 'IShallWearMidnight.jpg',
  },
  {
    title: 'Snuff',
    author: 'Terry Pratchett',
    photoPath: 'Snuff.jpg',
  },
  {
    title: 'Raising Steam',
    author: 'Terry Pratchett',
    photoPath: 'RaisingSteam.jpg',
  },
  {
    title: 'The Shepherd\'s Crown',
    author: 'Terry Pratchett',
    photoPath: 'TheShepherdCrown.jpeg',
  },
  {
    title: 'Nine Princes in Amber',
    author: 'Roger Zelazny',
    photoPath: 'Nine_princes_in_amber.jpg',
  },
  {
    title: 'The Guns of Avalon',
    author: 'Roger Zelazny',
    photoPath: 'Guns_of_avalon.jpg',
  },
  {
    title: 'Sign of the Unicorn',
    author: 'Roger Zelazny',
    photoPath: 'Sign_of_the_unicorn.jpg',
  },
  {
    title: 'The Hand of Oberon',
    author: 'Roger Zelazny',
    photoPath: 'Hand_of_oberon.jpg',
  },
  {
    title: 'The Courts of Chaos',
    author: 'Roger Zelazny',
    isBorrowed: true,
    userId: 4,
    photoPath: 'Courts_of_chaos.jpg',
  },
  {
    title: 'Trumps of Doom',
    author: 'Roger Zelazny',
    photoPath: 'Trumps_of_doom.jpg',
  },
  {
    title: 'Blood of Amber',
    author: 'Roger Zelazny',
    photoPath: 'Blood_of_amber_trade.jpg',
  },
  {
    title: 'Sign of Chaos',
    author: 'Roger Zelazny',
    photoPath: 'Sign_of_chaos.jpg',
  },
  {
    title: 'Knight of Shadows',
    author: 'Roger Zelazny',
    isBorrowed: true,
    userId: 1,
    photoPath: 'Knight_of_shadows.jpg',
  },
  {
    title: 'Prince of Chaos',
    author: 'Roger Zelazny',
    photoPath: 'Prince_of_chaos.jpg',
  },
  {
    title: 'Metro 2033',
    author: 'Dmitry Glukhovsky',
  },
  {
    title: 'Metro 2034',
    author: 'Dmitry Glukhovsky',
    photoPath: 'Metro2034.jpg',
  },
  {
    title: 'Metro 2035',
    author: 'Dmitry Glukhovsky',
    photoPath: 'Metro2035.jpg',
  },
  {
    title: 'The Bread We Eat in Dreams',
    author: 'Catherynne M. Valente',
    photoPath: 'TheBreadWeEatInDreams.jpg',
  },
  {
    title: 'Long December',
    author: 'Richard Chizmar',
    photoPath: 'LongDecember.jpg',
  },
  {
    title: 'Excalibur',
    author: 'Bernard Cornwell',
    photoPath: 'Excalibur.jpg',
  },
  {
    title: 'The Winter King',
    author: 'Bernard Cornwell',
    photoPath: 'TheWinterKing.jpg',
  },
  {
    title: 'Enemy of God',
    author: 'Bernard Cornwell',
    photoPath: 'EnemyOfGod.jpg',
  },
  {
    title: 'Zen and the Art of Motorcycle Maintenance',
    author: '	Robert M. Pirsig',
    photoPath: 'Zen-and-the-Art-of-MotorcycleMaintenance.jpg',
  },
  ];

  for (const book of books) {
    const foundBook = await booksRepository.findOne({
      title: book.title,
      author: book.author,
    });

    if (!foundBook) {
      const bookToCreate = booksRepository.create(book);
      console.log(await booksRepository.save(bookToCreate));
    }
  }
};

const seedsReviews = async (connection: any) => {
  //insert in reviews
  const reviewsRepository: Repository<Reviews> = connection.manager.getRepository(Reviews);

  const reviews = [
    {
      review: 'The best book ever!',
      bookId: 1,
      userId: 1,
    },
    {
      review: 'My favourite book in the whole world!',
      bookId: 1,
      userId: 2,
    },
    {
      review: `Reading The Color of Magic is akin to eating an entire bowl of ice cream just a little too fast...sure, it may cause your head to hurt at times, but the sweet rewards make it all worth it!
        Filled with ambitious wizards and ruthless assassins, the city of Ankh- Morpork has survived many dangers in the past, but now it faces an even more destructive force...TOURISM!!! When a rich but bored outsider named Twoflower decides to explore the city in search for adventure, it soon becomes an adventure for everyone around him, too! Twoflower's well-meaning but careless ways earn him the attention of pirates, dragonriders, and various supernatural entities, all looking to rid Twoflower of his treasure...not to mention his life! Soon failed wizard Rincewind reluctantly becomes Twoflower's guide, and as Twoflower explores more and more of Discworld looking for the adventure of a lifetime, Rincewind tries desperately to make sure his lifetime lasts for more than five minutes!`,
      bookId: 1,
      userId: 3,
    },
    {
      review: 'The best book ever!',
      bookId: 1,
      userId: 4,
    },
    {
      review: 'One of my favourite books!',
      bookId: 2,
      userId: 3,
    },
    {
      review: 'Fantastic book!',
      bookId: 2,
      userId: 2,
    },
    {
      review: 'Just great!',
      bookId: 3,
      userId: 1,
    },
  ];

  for (const review of reviews) {
    const foundReview = await reviewsRepository.findOne({
      bookId: review.bookId,
      userId: review.userId,
    });

    if (!foundReview) {
      const reviewToCreate = reviewsRepository.create(review);
      console.log(await reviewsRepository.save(reviewToCreate));
    }
  }
};

const seedsRate = async (connection: any) => {
  //insert in rates
  const rateRepository: Repository<Rate> = connection.manager.getRepository(Rate);

  const rates = [
    {
      rate: 4,
      bookId: 1,
      userId: 1,
    },
    {
      rate: 5,
      bookId: 1,
      userId: 2,
    },
    {
      rate: 4,
      bookId: 1,
      userId: 4,
    },
    {
      rate: 5,
      bookId: 2,
      userId: 1,
    },
    {
      rate: 2,
      bookId: 2,
      userId: 2,
    },
    {
      rate: 3,
      bookId: 3,
      userId: 1,
    },
    {
      rate: 4,
      bookId: 4,
      userId: 1,
    },
    {
      rate: 4,
      bookId: 5,
      userId: 2,
    },
    {
      rate: 4,
      bookId: 6,
      userId: 2,
    },
    {
      rate: 5,
      bookId: 7,
      userId: 1,
    },
    {
      rate: 3,
      bookId: 7,
      userId: 2,
    },
    {
      rate: 5,
      bookId: 8,
      userId: 1,
    },
    {
      rate: 5,
      bookId: 9,
      userId: 1,
    },
    {
      rate: 4,
      bookId: 10,
      userId: 1,
    },
  ];

  for (const rate of rates) {
    const foundRate = await rateRepository.findOne({
      bookId: rate.bookId,
      userId: rate.userId,
    });

    if (!foundRate) {
      const rateToCreate = rateRepository.create(rate);
      console.log(await rateRepository.save(rateToCreate));
    }
  }
};

const seeds = async () => {
  console.log('Seeds started!');
  const connection = await createConnection();

  try {
    await seedsUsers(connection);
    await seedsBooks(connection);
    await seedsReviews(connection);
    await seedsRate(connection);
  } catch (e) {
    console.log(e.message);
  }

  console.log('Seeds completed!');
  connection.close();
};

seeds().catch(console.error);
