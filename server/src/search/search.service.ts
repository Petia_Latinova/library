import { BooksTransformService } from './../books/books-transform.service';
import { Books } from './../books/books.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SearchService {
  constructor(
    @InjectRepository(Books)
    private readonly booksRepository: Repository<Books>,
    private readonly transform: BooksTransformService,
  ) {}

  async find(keyword: string, type: string) {
    const result: any = {
      type,
    };
    if (type === 'all' || type === 'books') {
      result.books = (
        await this.booksRepository.find({
          where: {
            text: `%${keyword}%`,
          },
        })
      ).map(b => this.transform.toBooksDTO(b));
    }

    return result;
  }
}
