import { Controller, Get, Query } from '@nestjs/common';
import { SearchService } from './search.service';

@Controller('books')
export class SearchController {
  constructor(private readonly searchService: SearchService) {}

  @Get()
  async find(@Query() query: { keyword: string; type: string }) {
    return await this.searchService.find(
      query.keyword || '',
      query.type || 'all',
    );
  }
}
