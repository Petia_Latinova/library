import { Controller, UseGuards, Get, Param, Post, Put, Body, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from './../users/passport/roles.guard';
import { UserRole } from './../users/passport/users-role';
import { ReviewsService } from './../reviews/reviews.service';
import { ReviewsDTO } from './../reviews/reviews.dto';
import { ReviewsInputDataDTO } from './../reviews/reviews-input-data.dto';
import { UserId } from './../users/passport/user-id.decorator';
import { BlacklistGuard } from './../users/passport/blacklist.guard';

@Controller('admin/books/:id/reviews')
  @UseGuards(BlacklistGuard, AuthGuard('jwt'), new RolesGuard(UserRole.Admin))
export class AdminReviewsController {
  constructor(private readonly reviewsService: ReviewsService) { }

  @Get()
  public async getBookReviews(@Param('id') bookId: string): Promise<ReviewsDTO[]> {
    return await this.reviewsService.getBookReviews(Number(bookId));
  }

  @Post()
  public async createBookReview(@Param('id') bookId: number,
    @Body() review: ReviewsInputDataDTO,@UserId() userId: number): Promise<ReviewsDTO> {
      return await this.reviewsService.createBookReview(bookId, review, userId, true);
  }

  @Put(':reviewId')
  public async updateBookReview(@Param('reviewId') reviewId: number,
    @Body() review: ReviewsInputDataDTO, @UserId() userId: number): Promise<ReviewsDTO> {
      return await this.reviewsService.updateBookReview(reviewId, review, userId, true);
  }

  @Delete(':reviewId')
  public async deleteBookReview(@Param('id') bookId: number, @Param('reviewId') reviewId: number,
    @UserId() userId: number): Promise<ReviewsDTO> {
      return await this.reviewsService.deleteBookReview(bookId, reviewId, userId, true);
  }

}
