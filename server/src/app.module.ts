import { BorrowHistory } from './books/borrow-history.entity';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AdminBooksController } from './admin-books/admin-books.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Votes } from './votes/vote.entity';
import { Reviews } from './reviews/reviews.entity';
import { Books } from './books/books.entity';
import { Users } from './users/users.entity';
import { Rate } from './rate/rate.entity';
import { BooksService } from './books/books.service';
import { BooksTransformService } from './books/books-transform.service';
import { RateService } from './rate/rate.service';
import { RateTransformService } from './rate/rate-transform.service';
import { ReviewsService } from './reviews/reviews.service';
import { ReviewsTransformService } from './reviews/reviews-transform.service';
import { UsersService } from './users/users.service';
import { UsersTransformService } from './users/users-transform.service';
import { VotesService } from './votes/votes.service';
import { VotesTransformService } from './votes/votes-transform.service';
import { AdminBooksService } from './admin-books/admin-books.service';
import { AdminReviewsController } from './admin-reviews/admin-reviews.controller';
import { BooksController } from './books/books.controller';
import { RateController } from './rate/rate.controller';
import { ReviewsController } from './reviews/reviews.controller';
import { UsersController } from './users/users.controller';
import { VotesController } from './votes/votes.controller';
import { AdminUsersService } from './admin-users/admin-users.service';
import { AdminUsersController } from './admin-users/admin-users.controller';
import { UsersBan } from './admin-users/users-ban.entity';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { jwtConstants } from './users/passport/secret';
import { JwtStrategy } from './users/passport/strategy';
import { Token } from './users/token.entity';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    MulterModule.register({
      dest: '/images',
    }),
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'librarydb',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      Votes,
      Reviews,
      Books,
      Users,
      Rate,
      UsersBan,
      BorrowHistory,
      Token,
    ]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      }
    }),
  ],
  controllers: [
    AppController,
    AdminBooksController,
    AdminReviewsController,
    BooksController,
    RateController,
    ReviewsController,
    UsersController,
    VotesController,
    AdminUsersController,
  ],
  providers: [
    AppService,
    BooksService,
    BooksTransformService,
    RateService,
    RateTransformService,
    ReviewsService,
    ReviewsTransformService,
    UsersService,
    UsersTransformService,
    VotesService,
    VotesTransformService,
    AdminBooksService,
    AdminUsersService,
    JwtStrategy,
  ],
})
export class AppModule {}
