import { RateService } from './rate.service';
import { Controller, Put, Param, Body, UseGuards, ValidationPipe } from '@nestjs/common';
import { RateDTO } from './rate.dto';
import { RateInputDataDTO } from './rate-input-data.dto';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from './../users/passport/blacklist.guard';
import { UserId } from './../users/passport/user-id.decorator';

@Controller('books/:id/rate')
  @UseGuards(BlacklistGuard, AuthGuard('jwt'))
export class RateController {
  constructor(private readonly rateService: RateService) {}

  @Put()
  public async rateBook(
    @Param('id') bookId: number,
    @Body(new ValidationPipe({ whitelist: true })) rate: RateInputDataDTO,
    @UserId() userId: number,
  ): Promise<RateDTO> {
    return await this.rateService.updateBookRates(bookId, rate, userId);
  }
}
