import { Users } from './../users/users.entity';
import { Books } from './../books/books.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity('rate')
export class Rate {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  rate: number;

  @ManyToOne(() => Books)
  book: Books;

  @Column({ nullable: true })
  bookId: number;

  @ManyToOne(() => Users)
  user: Users;

  @Column({ nullable: true })
  userId: number;
}
