import { UsersTransformService } from '../users/users-transform.service';
import { Rate } from './rate.entity';
import { RateDTO } from './rate.dto';
import { Inject } from '@nestjs/common';

export class RateTransformService {
  constructor(
    @Inject(UsersTransformService)
    private readonly userTransformService: UsersTransformService,
  ) {}

  toRateDTO(rate: Rate): RateDTO {
    return {
      id: rate.id,
      rate: rate.rate,
      User: this.userTransformService.toUserDTO(rate.user),
    };
  }
}
