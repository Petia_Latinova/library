import { BooksService } from './../books/books.service';
import { RateTransformService } from './rate-transform.service';
import { Rate } from './rate.entity';
import { RateDTO } from './rate.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RateInputDataDTO } from './rate-input-data.dto';
import { ReviewsService } from './../reviews/reviews.service';
import { AdminUsersService } from 'src/admin-users/admin-users.service';

@Injectable()
export class RateService {

  private static RATE_NOT_FOUND_EXCEPTION = `No rate found`;

  constructor(
    @InjectRepository(Rate) private readonly rateRepository: Repository<Rate>,
    private readonly rateTransformService: RateTransformService,
    private readonly booksService: BooksService,
    private readonly reviewsService: ReviewsService,
    private readonly adminUsersService: AdminUsersService,
  ) {}

  public async updateBookRates(
    id: number,
    rateInput: RateInputDataDTO,
    userId: number,
  ): Promise<RateDTO> {
    await this.adminUsersService.doesUserBanned(userId);
    await this.booksService.getById(id);
    await this.reviewsService.findReviewByUserIdAndBookId(userId, id);

    let rates: Rate;
    try {
      rates = await this.findRateByUserIdAndBookId(userId, id);
      rates.rate = rateInput.rate;
    } catch (e) {
      rates = this.rateRepository.create(rateInput);
      rates.bookId = (await this.booksService.getById(id)).id;
      rates.userId = userId;
    }

    const ratingBook = await this.rateRepository.save(rates);

    return this.rateTransformService.toRateDTO(
      await this.rateRepository.findOne({
        relations: ['book', 'user'],
        where: { id: ratingBook.id },
      }),
    );
  }

  public async findRateByUserIdAndBookId(
    userId: number,
    bookId: number,
  ): Promise<Rate> {
    const foundRate: Rate = await this.rateRepository.findOne({
      userId: userId,
      bookId: bookId,
    });
    if (foundRate === undefined) {
      throw new NotFoundException(RateService.RATE_NOT_FOUND_EXCEPTION);
    }
    return foundRate;
  }

}
