import { IsInt, Min, Max } from "class-validator";

export class RateInputDataDTO {
  @IsInt()
  @Min(1)
  @Max(5) 
  rate: number;
}