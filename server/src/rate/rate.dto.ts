import { UsersDTO } from "./../users/users.dto";

export class RateDTO {
  id: number;
  rate: number;
  User: UsersDTO;
}