import { Users } from './../users/users.entity';
import { Injectable, NotFoundException, ForbiddenException } from '@nestjs/common';
import { UsersInputBanDTO } from './users-input-ban.dto';
import { UsersService } from './../users/users.service';
import { UsersBan } from './users-ban.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, MoreThan } from 'typeorm';
import { UsersTransformService } from './../users/users-transform.service';
import { UsersDTO } from './../users/users.dto';
import * as moment from 'moment';

@Injectable()
export class AdminUsersService {
  private static readonly USER_BANNED_READING_POINTS = -1;

  private static readonly USER_DATE_NOT_VALID_ERROR = `Date must be in the future`;
  private static readonly USER_BANNED_NOT_FOUND_ERROR = `Banned user not found.`;
  private static readonly USER_BANNED_ERROR = `You are banned and can't perform that action!`;

  constructor(
    @InjectRepository(UsersBan) private readonly usersBanRepository: Repository<UsersBan>,
    @InjectRepository(Users) private readonly usersRepository: Repository<Users>,
    private readonly usersService: UsersService,
    private readonly usersTransformService: UsersTransformService,
  ) {}

  public async banUser( id: number, usersInput: UsersInputBanDTO ): Promise<UsersDTO> {
    const date = moment(usersInput.banned);
    if (!date.isValid || date.toDate() <= new Date()) {
      throw new ForbiddenException(AdminUsersService.USER_DATE_NOT_VALID_ERROR);
    }
    const user = await this.usersService.findUserById(id);

    let userBan: UsersBan;

    try { //update ban
      userBan = await this.findUserBan(id); // try to get user ban from DB
      userBan.banned = date.toDate(); //update the existing user ban
      userBan.description = usersInput.description;
    } catch (e) { //create new ban
      userBan = this.usersBanRepository.create(usersInput);
      userBan.userId = id;
      userBan.banned = date.toDate();
    }

    await this.usersBanRepository.save(userBan);

    await this.usersService.updateReadingPoints( id, AdminUsersService.USER_BANNED_READING_POINTS );

    return this.usersTransformService.toUserDTO(user);
  }

  public async findUserBan(userId: number): Promise<UsersBan> {
    const userBan: UsersBan = await this.usersBanRepository.findOne({ userId: userId });
    if (userBan === undefined) {
      throw new NotFoundException( AdminUsersService.USER_BANNED_NOT_FOUND_ERROR );
    }
    return userBan;
  }

  public async doesUserBanned(userId: number): Promise<boolean> {

    const userBan: UsersBan = await this.usersBanRepository.findOne({ userId: userId, banned: MoreThan(new Date())});

    if (userBan !== undefined) {
      throw new ForbiddenException(AdminUsersService.USER_BANNED_ERROR);
    }
    return true;
  }

  public async deleteUser(userId: number): Promise<{ msg: string }> {
    const userDel = await this.usersService.findUserById(userId);

    userDel.isDeleted = true;

    await this.usersRepository.save(userDel);

    await this.usersRepository
      .createQueryBuilder()
      .update(Users)
      .set({ isDeleted: true })
      .where('id = :id', { id: userId })
      .execute();

    return {
      msg: 'User has been deleted!',
    };
  }

  public async getAllUsers(): Promise<UsersDTO[]> {
    const users = await this.usersRepository.find({ isDeleted: false });

    return users.map(e => this.usersTransformService.toUserDTO(e));
  } 

}
