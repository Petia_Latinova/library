import { Controller, Put, Param, Body, UseGuards, Delete, Get, Query } from '@nestjs/common';
import { UsersDTO } from './../users/users.dto';
import { UsersInputBanDTO } from './users-input-ban.dto';
import { AdminUsersService } from './admin-users.service';
import { RolesGuard } from './../users/passport/roles.guard';
import { UserRole } from './../users/passport/users-role';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from './../users/passport/blacklist.guard';

@Controller('/admin/users')
@UseGuards(BlacklistGuard, AuthGuard('jwt'), new RolesGuard(UserRole.Admin))
export class AdminUsersController {
  
  constructor(private readonly adminUsersService: AdminUsersService) {}

  @Put(':id/banstatus')
  public async banUser(
    @Param('id') userId: number,
    @Body() usersInput: UsersInputBanDTO,
  ): Promise<UsersDTO> {
    return await this.adminUsersService.banUser(userId, usersInput);
  }

  @Delete(':id')
  public async deleteUser(
    @Param('id') userId: number,
  ): Promise<{ msg: string }> {
    return await this.adminUsersService.deleteUser(userId);
  }

  @Get()
  public async getAllUsers(@Query('name') name: string): Promise<UsersDTO[]> {
    const users = await this.adminUsersService.getAllUsers();

    if (name) {
      return users.filter(user => user.name.toLowerCase().includes(name));
    }   
    return users;
  }
  
}
