import { Length, IsNotEmpty } from "class-validator";

export class UsersInputBanDTO {
  @IsNotEmpty()
  banned: string;
  @IsNotEmpty()
  @Length(1, 1000)
  description: string;
}