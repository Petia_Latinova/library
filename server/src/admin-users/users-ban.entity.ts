import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Users } from '../users/users.entity';

@Entity('usersban')
export class UsersBan {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  banned: Date;

  @Column('nvarchar', { length: 1000 })
  description: string;

  @OneToOne(() => Users,
    user => user.id,
  )
  @JoinColumn()
  user: Users;

  @Column({ nullable: true })
  userId: number;
}
