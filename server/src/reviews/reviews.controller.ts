import { Controller, Get, Param, Post, Body, Put, Delete, UseGuards, ValidationPipe } from '@nestjs/common';
import { ReviewsService } from './reviews.service';
import { ReviewsDTO } from './reviews.dto';
import { ReviewsInputDataDTO } from './reviews-input-data.dto';
import { AuthGuard } from '@nestjs/passport';
import { UserId } from './../users/passport/user-id.decorator';
import { BlacklistGuard } from './../users/passport/blacklist.guard';

@Controller('books/:id/reviews')
@UseGuards(BlacklistGuard, AuthGuard('jwt'))
export class ReviewsController {
  constructor( private readonly reviewsService: ReviewsService ) { }

  @Get()
  public async getBookReviews(@Param('id') bookId: string): Promise<ReviewsDTO[]> {
    return await this.reviewsService.getBookReviews(Number(bookId));
  }

  @Post()
  public async createBookReview(@Param('id') bookId: number,
    @Body(new ValidationPipe({ whitelist: true })) review: ReviewsInputDataDTO, @UserId() userId: number): Promise<ReviewsDTO> {
      
      return await this.reviewsService.createBookReview(bookId, review, userId, false);
  }

  @Put(':reviewId')
  public async updateBookReview(@Param('reviewId') reviewId: number,
    @Body(new ValidationPipe({ whitelist: true })) review: ReviewsInputDataDTO, @UserId() userId: number): Promise<ReviewsDTO> {
      
      return await this.reviewsService.updateBookReview(reviewId, review, userId, false);
  }

  @Delete(':reviewId')
  public async deleteBookReview(@Param('id') bookId: number, @Param('reviewId') reviewId: number,
    @UserId() userId: number): Promise<ReviewsDTO> {
      
      return await this.reviewsService.deleteBookReview(bookId, reviewId, userId, false);
  }

}
