import { UsersDTO } from "./../users/users.dto";
import { BooksDTO } from "./../books/books.dto";
import { VotesDTO } from "./../votes/votes.dto";

export class ReviewsDTO {
  id: number;
  review: string;
  Book: BooksDTO;
  User: UsersDTO;
  Votes: VotesDTO[];
}