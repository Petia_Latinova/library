import { Reviews } from "./reviews.entity";
import { ReviewsDTO } from "./reviews.dto";
import { Inject } from "@nestjs/common";
import { UsersTransformService } from '../users/users-transform.service';
import { BooksTransformService } from "./../books/books-transform.service";
import { VotesTransformService } from "./../votes/votes-transform.service";

export class ReviewsTransformService {

  constructor(
    @Inject(UsersTransformService) private readonly userTransformService: UsersTransformService,
    @Inject(BooksTransformService) private readonly booksTransformService: BooksTransformService,
    @Inject(VotesTransformService) private readonly votesTransformService: VotesTransformService
  ) {}

  toReviewsDTO(review: Reviews): ReviewsDTO {
    return {
      id: review.id,
      review: review.review,
      Book: this.booksTransformService.toBooksDTO(review.book),
      User: this.userTransformService.toUserDTO(review.user),
      Votes: review.votes.map(e => this.votesTransformService.toVoteDTO(e)),
    }
  }

}
