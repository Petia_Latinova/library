import { ReviewsService } from './reviews.service';
import { ReviewsTransformService } from './reviews-transform.service';
import { NotFoundException } from '@nestjs/common';
describe('ReviewsService', () => {

  it('should initialize', () => {

    const service = new ReviewsService(null, null, null, null, null);

    expect(service).toBeDefined();
  });

  it('getBookReviews', () => {

    const reviewsRepositoryMock = {
      async find() {
        return [{
          "id": 1,
          "review": "The best book ever!",
          "Book": {
            "id": 1,
            "title": "",
            "author": "",
            "isBorrowed": true,
            "Rate": []
          },
          "User": {
            "id": 1,
            "name": "Petia"
          },
          "Votes": []
        }];
      }
    };

    const service = new ReviewsService(reviewsRepositoryMock as any, ReviewsTransformService as any, null, null, null);
    jest.spyOn(reviewsRepositoryMock, 'find');

    service.getBookReviews(1);

    expect(reviewsRepositoryMock.find).toHaveBeenCalledWith({
      relations: ['book', 'book.rates', 'user', 'votes'],
      where: { bookId: 1, isDeleted: false },
    });
  });

  it('getBookReviews with wrong id', () => {

    const reviewsRepositoryMock = {
      async find() {
        return [{
          "id": 1,
          "review": "The  best book ever!",
          "Book": {
            "id": 1,
            "title": "",
            "author": "",
            "isBorrowed": true,
            "Rate": []
          },
          "User": {
            "id": 1,
            "name": "Petia"
          },
          "Votes": []
        }];
      }
    };

    const service = new ReviewsService(reviewsRepositoryMock as any, ReviewsTransformService as any, null, null, null);
    jest.spyOn(reviewsRepositoryMock, 'find');

    service.getBookReviews(2);

    expect(reviewsRepositoryMock.find).not.toHaveBeenCalledWith({
      relations: ['book', 'book.rates', 'user', 'votes'],
      where: { bookId: 1, isDeleted: false },
    });
  });

  it('getBookReviews empty result', () => {

    const reviewsRepositoryMock = {
      async find() {
        return [];
      }
    };

    const service = new ReviewsService(reviewsRepositoryMock as any, ReviewsTransformService as any, null, null, null);
    jest.spyOn(reviewsRepositoryMock, 'find');

    try {
      service.getBookReviews(2);
    }
    catch(e) {
      expect(e).toBe(NotFoundException);
    }

    
  });

});