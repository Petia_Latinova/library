import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Books } from './../books/books.entity';
import { Users } from './../users/users.entity';
import { Votes } from './../votes/vote.entity';

@Entity('reviews')
export class Reviews {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('nvarchar', { length: 1000 })
  review: string;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @ManyToOne(
    () => Books,
    book => book.reviews,
  )
  book: Books;

  @Column({ nullable: true })
  bookId: number;

  @ManyToOne(() => Users)
  user: Users;

  @Column({ nullable: true })
  userId: number;

  @OneToMany(
    () => Votes,
    vote => vote.review,
  )
  votes: Votes[];
}
