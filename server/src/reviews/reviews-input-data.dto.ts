import { Length, IsNotEmpty } from "class-validator";

export class ReviewsInputDataDTO {
  @IsNotEmpty()
  @Length(2, 1000)
  review: string;
}