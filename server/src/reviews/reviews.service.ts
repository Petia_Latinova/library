import { Injectable, NotFoundException, ForbiddenException } from '@nestjs/common';
import { ReviewsDTO } from './reviews.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Reviews } from './reviews.entity';
import { ReviewsTransformService } from './reviews-transform.service';
import { Repository } from 'typeorm';
import { ReviewsInputDataDTO } from './reviews-input-data.dto';
import { BooksService } from './../books/books.service';
import { AdminUsersService } from './../admin-users/admin-users.service';
import { UsersService } from './../users/users.service';

@Injectable()
export class ReviewsService {
  private static readonly CREATE_REVIEW_READING_POINTS = 2;

  private static readonly REVIEW_NOT_FOUND_ERROR = `Review not found.`;
  private static readonly REVIEW_CANT_UPDATE_ERROR = `Can't update other users reviews.`;
  private static readonly REVIEW_ALREADY_EXIST_ERROR = `Review already exist.`;

  constructor(
    @InjectRepository(Reviews) private readonly reviewsRepository: Repository<Reviews>,
    private readonly reviewsTransformService: ReviewsTransformService,
    private readonly booksService: BooksService,
    private readonly adminUsersService: AdminUsersService,
    private readonly usersService: UsersService,
  ) {}

  public async getBookReviews(id: number): Promise<ReviewsDTO[]> {
    const reviews: ReviewsDTO[] = ( await this.reviewsRepository.find({
      relations: ['book', 'book.rates', 'book.rates.user', 'user', 'votes', 'votes.user'],
      where: { bookId: id, isDeleted: false },
      order: { id: 'DESC' }
    })).map(e => this.reviewsTransformService.toReviewsDTO(e));
    
    /*if (reviews.length === 0) {
      throw new NotFoundException(ReviewsService.REVIEW_NOT_FOUND_ERROR);
    }*/
    return reviews;
  }

  public async createBookReview(id: number, reviewInput: ReviewsInputDataDTO, userId: number, isAdmin: boolean): Promise<ReviewsDTO> {
    await this.adminUsersService.doesUserBanned(userId);
    let oldReview: Reviews;

    try { // check does review exist. If goes in the catch there is no review and we'll add one
      oldReview = await this.findReviewByUserIdAndBookId(userId, id);
    }
    catch(e) {
      oldReview = null;
    }

    if (oldReview) { // if review exist throw exseption
      throw new ForbiddenException(ReviewsService.REVIEW_ALREADY_EXIST_ERROR);
    }

    if(!isAdmin) {
      await this.booksService.borrowHistory(userId, id);
    }

    const review: Reviews = this.reviewsRepository.create(reviewInput);
    review.bookId = (await this.booksService.getById(id)).id;
    review.userId = userId;
    const createdReview = await this.reviewsRepository.save(review); // take the new review with the created id in variable so can be used later

    await this.usersService.updateReadingPoints(userId, ReviewsService.CREATE_REVIEW_READING_POINTS );

    return this.reviewsTransformService.toReviewsDTO( await this.reviewsRepository.findOne({
      relations: ['book', 'book.rates', 'book.rates.user', 'user', 'votes', 'votes.user'],
      where: { id: createdReview.id }
    }));
  }

  public async updateBookReview(revid: number, reviewInput: ReviewsInputDataDTO, userId: number, isAdmin: boolean ): Promise<ReviewsDTO> {
    await this.adminUsersService.doesUserBanned(userId);

    const oldReview: Reviews = await this.findReviewById(Number(revid));
    oldReview.review = reviewInput.review;
    if (!isAdmin && oldReview.userId !== userId) {
      throw new ForbiddenException(ReviewsService.REVIEW_CANT_UPDATE_ERROR);
    }

    await this.reviewsRepository.save(oldReview);

    return this.reviewsTransformService.toReviewsDTO( await this.reviewsRepository.findOne({
      relations: ['book', 'book.rates', 'book.rates.user', 'user', 'votes', 'votes.user'],
      where: { id: revid, isDeleted: false }
    }));
  }

  public async deleteBookReview(bid: number, revid: number, userId: number, isAdmin: boolean ): Promise<ReviewsDTO> {
    await this.adminUsersService.doesUserBanned(userId);
    await this.booksService.getById(bid); //check does book exist
    const reviewForDel: Reviews = await this.findReviewById(Number(revid));

    if (!isAdmin && reviewForDel.userId !== userId) {
      throw new ForbiddenException(ReviewsService.REVIEW_CANT_UPDATE_ERROR);
    }

    reviewForDel.isDeleted = true;

    await this.reviewsRepository.save(reviewForDel);

    return this.reviewsTransformService.toReviewsDTO( await this.reviewsRepository.findOne({
      relations: ['book', 'book.rates', 'book.rates.user', 'user', 'votes', 'votes.user'],
      where: { id: revid }
    }));
  }

  public async findReviewById(reviewId: number): Promise<Reviews> {
    const foundReview: Reviews = await this.reviewsRepository.findOne({ id: reviewId, isDeleted: false });
    if (foundReview === undefined || foundReview.isDeleted) {
      throw new NotFoundException(ReviewsService.REVIEW_NOT_FOUND_ERROR);
    }
    return foundReview;
  }

  public async findReviewByUserIdAndBookId( userId: number, bookId: number ): Promise<Reviews> {
    const foundReview: Reviews = await this.reviewsRepository.findOne({ userId: userId, bookId: bookId, isDeleted: false });
    if (foundReview === undefined || foundReview.isDeleted) {
      throw new NotFoundException(ReviewsService.REVIEW_NOT_FOUND_ERROR);
    }
    return foundReview;
  }
  
}
