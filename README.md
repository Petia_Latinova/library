## Library

<br/>

**Users**
- id (int, because it's simpler)
- username - "admin"
- password - "123456aA!"
- role - admin or basic
- readingPoints
- avatarPath - photo for user
- isDeleted - set to true if user is deleted  
<br/> 

**Books** 
- id
- title
- author 
- isDelisted - set to true when book is not shown in the library
- isBorrowed - if borrowed a book
- userId
- PhotoPath - cover for book
  
<br/>  
  
**Borrowhistory** - when book is returned add a record
- id
- bookId
- userId
- date
  
<br />
<br /> 
    
**Reviews**
- id
- review - reviews text
- isDeleted
- bookId
- userId
  
<br/>

**Rate**
- id
- rate - from 1 to 5 for a book
- bookId
- userId
  
<br/>

**Votes**
- id
- liked - like/dislike
- reviewId
- userId
  
<br/>

**Usersban**
- id
- banned - until which date the user is banned
- description
- userId
  
<br/>  

**Token** - when logout blacklist the token, so if somebody steal it not to be able to log in with others token 
- id
- token
- blacklistedOn
  
<br/> 

#### **Overview**
**1.** We can create users (register), we can edit books, we can search for books by author or title and we can see the rate for each book.  
**2.** We can create reviews, we can change our own reviews and we can like/dislike others reviews, we can borrow and return a book.  
**3.** We can see reviews and from which user is the review, we can vote and unvote review  
**4.** We can see other users points and rank  
**5.** There is admin option. The admin can ban users or other admins for period of time. Can CRUD reviews and books.  

<br />
  
#### **Endpoints**

- **POST /api/users** - {name, password} - creates with those required properties
- **POST /api/users/login** - {name, password} - login with those required properties

- **GET /api/books** - see all oft the books in the library
- **GET /api/books/:id** - see individual book
- **POST /api/books/:id** - borrow a book - boolean
- **DELETE /api/books/:id** - return a book - boolean
  
- **GET /api/books/:id/reviews** - see reviews for individual book
- **POST /api/books/:id/reviews** - {review} - add review
- **PUT api/books/:id/reviews/:reviewId** - {review} - edit your own review
- **DELETE /api/books/:id/reviews/:reviewId** - delete your own review

- **PUT api/books/:id/rate** - {rate} - rate a book
- **PUT /api/reviews/:reviews/:id/votes** - {liked} - vote for a book


- **PUT /api/admin/users/:id/banstatus** - {banned, description} - if you are admin ban users for period of time
  - if banned user have noly read rights 
  
- **GET /api/admin/books/:id** - <span style="color: red">??</span>
- **POST /api/admin/books** - {author, title} - if you are admin add individual book
- **PUT /api/admin/books** - {author, title} - if you are admin edit individual book
- **DELETE /api/admin/books/:id** - if you are admin delete individual book
- <span style="color: red">?? delete user?</span>
  
- **GET /api/admin/books/:id/reviews** - if you are admin see reviews - ???
- **Put /api/admin/books/:id/reviews/:id** - {review} if you are admin update any review
- **DELETE /api/admin/books/:id/reviews/:id** - if you are admin delete any review
- 
- **DELETE /api/users/logout** - user logout
- **GET /api/admin/users** - if you are admin see all users

<br />
  
### 1. **Project Requirements**

- install dependencies with `npm install`
- setup db configuration:

```js
{
    type: 'mariadb',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'root',
    database: 'librarydv',
    entities: ["dist/**/*.entity{.ts,.js}"],
    synchronize: true
}
```

- setup `ormconfig.json`

```json
{
  "type": "mysql",
  "host": "localhost",
  "port": 3306,
  "username": "root",
  "password": "root",
  "database": "librarydb",
  "synchronize": true,
  "logging": false,
  "entities": [
    "src/models/**/*.entity.ts"
  ],

  "cli": {
    "entitiesDir": "src/models",
    "migrationsDir": "src/data/migration"
  }
}
```

- generate seed data: `npm run seeds`

<br />
  
### 2. **Testing**

There are two users:

- Basic: username `basic`, password `123456aA!`
- Admin: username `admin`, password `123456aA!`
